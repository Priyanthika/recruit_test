import 'package:flutter/material.dart';

class SelectBox extends StatefulWidget {
  List<SelectBoxItem> items;
  var onChanged;
  var hint;
  var disabledHint;
  double iconSize;
  bool isDense;
  int selectItm;
  int enabledItem;
  bool whatIsTrue;

  //Constructor
  SelectBox(
      {
      // @required Key key,
      this.items,
      this.hint,
      this.disabledHint,
      this.iconSize,
      this.isDense,
      this.selectItm,
      this.enabledItem,
      this.whatIsTrue,
      this.onChanged})
  // : super(key: key)
  ;

  @override
  SelectBoxState createState() => SelectBoxState();
}

class SelectBoxState extends State<SelectBox> {
  List<SelectBoxItem> itemx;
  List<int> valid;
  SelectBoxItem selectedItem = null;

  var theme;

  @override
  void initState() {
    super.initState();
    selectedItem = widget.items[widget.selectItm];
    itemx = widget.items;
  }

  List<DropdownMenuItem<SelectBoxItem>> giveMeFeed() {
    List<DropdownMenuItem<SelectBoxItem>> xx = [];
    if (itemx != null && itemx.isNotEmpty && itemx.length !=0) {
      for (var i = 0; i < itemx.length; i++) {
        xx.add(DropdownMenuItem<SelectBoxItem>(
          value: widget.items[i],
          child: Row(
            children: <Widget>[
              Icon(
                itemx[i].icon,
                size: 10.0,
                color: itemx[i].clr,
              ),
              MediaQuery(
                data: MediaQueryData(),
                child: Text(
                  itemx[i].name,
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
              ),
            ],
          ),
        ));
      }
    }

    return xx;
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButtonHideUnderline(
      child: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Colors.white,
          hintColor: Colors.grey,
        ),
        child: DropdownButtonFormField<SelectBoxItem>(
          // items: items.map((SelectBoxItem item) {
          //   return DropdownMenuItem<SelectBoxItem>(
          //     value: item,
          //     child: Row(
          //       children: <Widget>[
          //         Icon(
          //           item.icon,
          //           size: 10.0,
          //           color: item.clr,
          //         ),
          //         MediaQuery(
          //           data: MediaQueryData(),
          //           child: Text(
          //             item.name,
          //             style: TextStyle(
          //               color: Colors.black,
          //             ),
          //           ),
          //         ),
          //       ],
          //     ),
          //   );
          // }).toList(),
          items: giveMeFeed(),
          value: selectedItem,
          onChanged: (SelectBoxItem item) {
            setState(() {
              selectedItem = item;
            });
            widget.onChanged(item);
          },
          hint: widget.hint,
          // onSaved: (item) {
          //   //Form save method
          // },
        ),
      ),
    );
  }
}

class SelectBoxItem {
  final String name;
  final int type;
  final IconData icon;
  final Color clr;
  SelectBoxItem(this.name, this.type, this.icon, this.clr);
}
