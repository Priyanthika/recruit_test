import 'package:tenizo/tennizo_base_controller.dart';
import 'package:flutter/material.dart';

import '../tennizo_controller_functions.dart';

class PrivacyPolicy extends StatefulWidget {
  // final arguments;
  // final Function onSelected;
  @override
  _PrivacyPolicyState createState() => _PrivacyPolicyState();
  // PrivacyPolicy({Key key, this.onSelected, this.arguments}) : super(key: key);
}

class _PrivacyPolicyState extends State<PrivacyPolicy>
    with BaseControllerListner {
  BaseController controller;

  @override
  void initState() {
    super.initState();
    controller = new BaseController(this);
  }

  _sendMail() async {
    String subject = "Inquiries regarding Privacy Policy";
    var param = {};

    param["to"] = "arit.inquiry@gmail.com";
    param["subject"] = subject.replaceAll(" ", "%20");
    param["body"] = "";

    controller.execFunction(
        ControllerFunc.launch_app, ControllerSubFunc.launch_mailer, param);
  }

  _titleArea() {
    return Column(
      children: <Widget>[
        Container(
          margin: const EdgeInsets.only(bottom: 15.0, top: 15),
          child: Text('TenniZo',
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 30.0)),
        ),
        Container(
          margin: const EdgeInsets.only(left: 15.0, right: 15.0),
          child: Text(
            'TenniZo is a tennis score entry application provided by Arit Co.,Ltd.and TAKUMI TECH (PVT) LTD (hereinafter referred to as "We"). \n \n'
            'In order to respect the importance of personal information and provide thorough protection when providing applications, we comply with applicable laws and regulations regarding the protection of personal information, and set personal information protection policy as follows. \n \n'
            'This the privacy policy applies to all activities related to Customer’s personal information. Therefore, in addition to being posted on our website, we will provide in our printed materials as needed.',
            textAlign: TextAlign.justify,
          ),
        ),
      ],
    );
  }

  _descArea() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
            margin: const EdgeInsets.only(bottom: 10.0, top: 25.0, left: 15),
            child: Text('1.Definition of Personal Information',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 19.0))),
        Container(
            margin: const EdgeInsets.only(
                bottom: 10.0, top: 5.0, left: 15, right: 15),
            child: Text(
              'Information such as Customer’s name, zip code, address, phone number, age, date of birth, gender, occupation, e-mail address, etc. who use our website (hereinafter referred to as "Website") will be identified as Personal Information.',
              textAlign: TextAlign.justify,
            )),
        Container(
            margin: const EdgeInsets.only(bottom: 10.0, top: 10.0, left: 15),
            child: Text('2.Cookies and IP address information',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 19.0))),
        Container(
            margin: const EdgeInsets.only(
                bottom: 10.0, top: 5.0, left: 15, right: 15),
            child: Text(
              'We do not consider cookies and IP address information as personal information as they cannot be used independently to identify a specific Customer. However, if such information and personal information are used together, such information is also regarded as personal information.',
              textAlign: TextAlign.justify,
            )),
        Container(
            margin: const EdgeInsets.only(bottom: 10.0, top: 10.0, left: 15),
            child: Text(
                '3. Identification of purpose using personal information',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 19.0))),
        Container(
            margin: const EdgeInsets.only(
                bottom: 10.0, top: 5.0, left: 15, right: 15),
            child: Text(
              'In handling personal information, we will specify the purpose of its use as much as possible.',
              textAlign: TextAlign.justify,
            )),
        Container(
            margin: const EdgeInsets.only(bottom: 10.0, top: 10.0, left: 15),
            child: Text('4. Limitation of using Personal Information',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 19.0))),
        Container(
          margin: const EdgeInsets.only(
              bottom: 10.0, top: 10.0, left: 15, right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'We do not handle personal information beyond the extent necessary to attain the purpose for which the personal information is used, without their respective prior consent. However, this shall not apply in the following cases:',
                textAlign: TextAlign.justify,
              ),
              Container(
                margin: const EdgeInsets.only(left: 15.0, top: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                        '1. Cases in which it is based on laws and regulations.'),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                          '2. Cases in which disclosure or provision of personal information is necessary for the protection of the life, body or property of a person, and it is difficult to obtain the consent of the customers.'),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                          '3. Cases in which the information is necessary for the improvement of public health or promotion of sound growth of children, but is difficult to obtain customer consent.'),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                          '4. Cases in which the provision of personal data is necessary for cooperating with a state organ, a local government, or a Customer or a business operator entrusted by one in executing the affairs prescribed by laws and regulations and in which obtaining the consent of the person is likely to impede the execution of the affairs.'),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
            margin: const EdgeInsets.only(bottom: 10.0, top: 10.0, left: 15),
            child: Text('5. Appropriate collection of personal information',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 19.0))),
        Container(
            margin: const EdgeInsets.only(
                bottom: 10.0, top: 5.0, left: 15, right: 15),
            child: Text(
              'We properly acquires personal information and does not acquire it by false or other fraudulent means. We will also keep in mind that we will not collect personally identifiable information from children under 16 years of age without the consent of a custodial person.',
              textAlign: TextAlign.justify,
            )),
        Container(
            margin: const EdgeInsets.only(bottom: 10.0, top: 10.0, left: 15),
            child: Text(
                '6. Notification of purpose of use when acquiring personal information',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 19.0))),
        Container(
          margin: const EdgeInsets.only(
              bottom: 10.0, top: 10.0, left: 15, right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'We will notify the purpose of use prior to acquiring personal information. However, this shall not apply in the following cases:',
                textAlign: TextAlign.justify,
              ),
              Container(
                margin: const EdgeInsets.only(left: 15.0, top: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                        '1. Cases in which there is a risk of harm to life, body, property or other rights or interests of the Customer or any third party by notifying of publicizing to Customer.'),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                          '2. Cases in which notifying or publicizing the purpose of use may harm our rights or legitimate interests.'),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                          '3. Cases when it is necessary to cooperate with an agency of the state or local government organization or their agent in the execution of their duty according to law, and obtaining the consent of the user would pose a risk of interfering with the execution of this duty.'),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                          '4. Cases where the purpose of use is found to be clear in light of the circumstances of the acquisition'),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
            margin: const EdgeInsets.only(bottom: 10.0, top: 10.0, left: 15),
            child: Text(
                '7.Changes in the purpose of use of personal information',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 19.0))),
        Container(
            margin: const EdgeInsets.only(
                bottom: 10.0, top: 5.0, left: 15, right: 15),
            child: Text(
              'When making changes to the purposes that personal information is used for our company, we will not use the information for purposes beyond that which would be rationally considered similar to the purposes before the change, and will publicize or communicate the changes to the persons affected.',
              textAlign: TextAlign.justify,
            )),
        Container(
            margin: const EdgeInsets.only(bottom: 10.0, top: 10.0, left: 15),
            child: Text(
                '8. Handling of personal information and supervision of employees',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 19.0))),
        Container(
            margin: const EdgeInsets.only(
                bottom: 10.0, top: 5.0, left: 15, right: 15),
            child: Text(
              'We sets the personal information protection regulations to prevent leakage, loss, or damage of personal information and to manage it safely, as well as carries out necessary and appropriate supervision of the employees.',
              textAlign: TextAlign.justify,
            )),
        Container(
            margin: const EdgeInsets.only(bottom: 10.0, top: 10.0, left: 15),
            child: Text('9. Supervision of Trustees',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 19.0))),
        Container(
            margin: const EdgeInsets.only(
                bottom: 10.0, top: 5.0, left: 15, right: 15),
            child: Text(
              'If we completely or partially delegates handling of personal information, we will enter into a contract that includes a non-disclosure contract or the terms and conditions set by us with the delegated party as well as carry out necessary and appropriate supervision of the delegated party to ensure the personal information will be safely managed.',
              textAlign: TextAlign.justify,
            )),
        Container(
            margin: const EdgeInsets.only(bottom: 10.0, top: 10.0, left: 15),
            child: Text(
                '10. Limitation of providing personal information to the third party',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 19.0))),
        Container(
          margin: const EdgeInsets.only(
              bottom: 10.0, top: 10.0, left: 15, right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'We shall not disclose any personal information to a third party under any circumstance without customer’s prior consent, except in the following cases:',
                textAlign: TextAlign.justify,
              ),
              Container(
                margin: const EdgeInsets.only(left: 15.0, top: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('1. Cases in which it’s based on the law.'),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                          '2. Cases in which disclosure or provision of personal information is necessary for the protection of the life, body or property of a person, and it is difficult to obtain the consent of the customers.'),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                          '3. Cases in which the information is necessary for the improvement of public health or promotion of sound growth of children, but is difficult to obtain customer consent.'),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                          '4. Cases in which the provision of personal data is necessary for cooperating with a state organ, a local government, or a Customer or a business operator entrusted by one in executing the affairs prescribed by laws and regulations and in which obtaining the consent of the person is likely to impede the execution of the affairs.'),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                          '5. If the following items have been announced or publicizing in advance:'),
                    ),
                    Container(
                      margin: const EdgeInsets.only( top: 10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left : 18.0),
                            child: Text(
                                '・If the provision to a third party is included in the purpose of use.'),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left : 18.0),
                            child: Text(
                                '・The items of the personal data to be provided to a third party.'),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left : 18.0),
                            child: Text(
                                '・The means or method of provision to a third party.'),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left : 18.0),
                            child: Text(
                                '・Stop providing personal information to the third party upon request of the Customer.'),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.only(
              bottom: 10.0, top: 10.0, left: 15, right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'However, none of the following is considered as a third party specified above:',
                textAlign: TextAlign.justify,
              ),
              Container(
                margin: const EdgeInsets.only(left: 15.0, top: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                        '1.  If we completely or partially delegates the handling of personal information within the scope necessary for the achievement of the purpose for usage.'),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                          '2. If personal information is transferred to the successor following the succession of business due to a merger or other reasons.'),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                          '3. If personal information is used in common with a specific person, and there has been prior notification to the user or the information has been made easily available to the user, of this fact, and of the items of personal information to be used in common, the range of the use in common, the purpose of use to which the person will put the use, and the name of the person or organization having responsibility for managing this personal information.'),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15.0),
                child: Text(
                  'If we share the personal information with any specific party, we will, in advance, notify the customers or make readily accessible the details of any change to the purpose for usage of personal information or any change to the name or title of any responsible person for managing the personal information.',
                  textAlign: TextAlign.justify,
                ),
              ),
            ],
          ),
        ),
        Container(
            margin: const EdgeInsets.only(bottom: 10.0, top: 10.0, left: 15),
            child: Text(
                '11. Publication of items relating to personal information',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 19.0))),
        Container(
          margin: const EdgeInsets.only(
              bottom: 10.0, top: 10.0, left: 15, right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'We will make the following items easily available to the Customer, and on request by the Customer will supply them without delay.',
                textAlign: TextAlign.justify,
              ),
              Container(
                margin: const EdgeInsets.only(left: 15.0, top: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                        '1. Purpose of use of personal information (However, Act on the Protection of Personal Information excludes the provision that there is no obligation. '
                        'If decide not to respond, notify the customer without delay to that effect. )'),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child:
                          Text('2. Inquiries related to personal information.'),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
            margin: const EdgeInsets.only(bottom: 10.0, top: 10.0, left: 15),
            child: Text('12. Disclosure of personal information',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 19.0))),
        Container(
          margin: const EdgeInsets.only(
              bottom: 10.0, top: 10.0, left: 15, right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'If the customer requests to disclose personal information, we will disclose it to the customer without delay. However, if the disclosure falls under any of the following, it may not be disclosed in whole or in part. \n \n'
                'If decided not to disclose, notify to that effect.',
                textAlign: TextAlign.justify,
              ),
              Container(
                margin: const EdgeInsets.only(left: 15.0, top: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                        '1. Cases in which there is a risk of harm to life, body, property or other rights or interests of the customer or any third party.'),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                          '2. If the fulfilment of the request threatens to cause material impediment to our business operation.'),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                          '3. If the fulfilment of the request constitutes a violation of laws and regulations.'),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15.0),
                child: Text(
                  'In addition, we will not disclose any information other than personal information such as access logs.',
                  textAlign: TextAlign.justify,
                ),
              ),
            ],
          ),
        ),
        Container(
            margin: const EdgeInsets.only(bottom: 10.0, top: 10.0, left: 15),
            child: Text('13. Amendment of personal information',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 19.0))),
        Container(
            margin: const EdgeInsets.only(
                bottom: 10.0, top: 5.0, left: 15, right: 15),
            child: Text(
              'If the customer requests a correction, addition, or deletion (hereinafter called “correction”) of any details of their personal information on the grounds that those details are false,'
              'except in cases in which any special procedure is prescribed by law or ordinance, we will carry out any necessary investigation without delay within the scope necessary for the achievement'
              'of the purpose of usage and, on the basis of the results, correct those details and notify the customer to that effect.',
              textAlign: TextAlign.justify,
            )),
        Container(
            margin: const EdgeInsets.only(bottom: 10.0, top: 10.0, left: 15),
            child: Text('14. Discontinuation of use of personal information',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 19.0))),
        Container(
            margin: const EdgeInsets.only(
                bottom: 10.0, top: 5.0, left: 15, right: 15),
            child: Text(
              'If the customer requests to discontinue using or to delete (hereinafter called “discontinue usage”) the personal information on the grounds that the information is being handled in a manner that exceeds '
              'the scope of the purpose for usage publicly announced in advance or because the information has been acquired through deception or any other wrongful means, we will carry out any necessary inspection of the '
              'information without delay and, on the basis of the results, discontinue usage of the information and notify the customer to that effect. \n \n'
              'However, if it is difficult to discontinue the use of personal information, we will take this alternative if we can take the necessary measures to protect the rights and interests of the person.',
              textAlign: TextAlign.justify,
            )),
        Container(
          margin: const EdgeInsets.only(
              bottom: 10.0, top: 10.0, left: 15, right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('15. Explanation of Reasons',
                  style:
                      TextStyle(fontWeight: FontWeight.w600, fontSize: 19.0)),
              Padding(
                padding: const EdgeInsets.only(top: 15.0),
                child: Text(
                  'We shall not do the following without Customer’s request:',
                  textAlign: TextAlign.justify,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 15.0, top: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('1. Notifying the purpose of the use'),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                          '2. Disclosing whole or part of the personal information'),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                          '3. Discontinuation of use of personal information'),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                          '4. Discontinuation of providing personal information to the third party'),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15.0),
                child: Text(
                  'If we decide on any of the above, we will explain the reason when notifying the person to that effect.',
                  textAlign: TextAlign.justify,
                ),
              ),
            ],
          ),
        ),
        Container(
            margin: const EdgeInsets.only(bottom: 10.0, top: 10.0, left: 15),
            child: Text('16. Inquiries',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 19.0))),
        Container(
            margin: const EdgeInsets.only(
                bottom: 10.0, top: 5.0, left: 15, right: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'For inquiries about personal information, you may contact us at: \n \n'
                  'Arit Co., Ltd. \n'
                  '3-3-22, Nishino 4-jo, Nishi-ku Sapporo-shi, Hokkaido, 063-0034, Japan \n'
                  'General affairs section \n'
                  'Person in charge of personal information protection management ',
                  textAlign: TextAlign.justify,
                ),
                FlatButton(
                  onPressed: _sendMail,
                  padding: const EdgeInsets.only(left: 0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          height: 50.0,
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "arit.inquiry@gmail.com",
                            style: TextStyle(color: Colors.blueAccent),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )),
        Container(
            margin: const EdgeInsets.only(
                bottom: 10.0, top: 5.0, left: 15, right: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'TAKUMI TECH (PVT) LTD \n'
                  '1/4-1/1, Mahindarama Mawatha, Colombo 10 \n'
                  'General affairs section \n'
                  'Person in charge of personal information protection management ',
                  textAlign: TextAlign.justify,
                ),
                FlatButton(
                  onPressed: _sendMail,
                  padding: const EdgeInsets.only(left: 0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          height: 50.0,
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "arit.inquiry@gmail.com",
                            style: TextStyle(color: Colors.blueAccent),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title:
                MediaQuery(data: MediaQueryData(), 
                child: Text('Privacy Policy',
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.w700))),
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back_ios),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ),
          body: MediaQuery(
            data: MediaQueryData(),
            child: SingleChildScrollView(
              //main body container
              child: Column(
                children: <Widget>[
                  _titleArea(),
                  _descArea(),
                ],
              ),
            ),
          )),
      onWillPop: () async => Future.value(false),
    );
  }

  @override
  resultFunction(func, subFunc, response) {
  }
}
