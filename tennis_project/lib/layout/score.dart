import 'dart:io';

import 'package:tenizo/const.dart';
import 'package:tenizo/tennizo_base_controller.dart';
import 'package:tenizo/tennizo_controller_functions.dart';
import 'package:flutter/material.dart';
import 'package:tenizo/util/app_const.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tenizo/util/app_popup.dart';
import 'dart:math';
import 'dart:async';
import 'package:star_menu/star_menu.dart';

class Score extends StatefulWidget {
  final Function funcSetGameStart;
  final Function onSelected;
  final Function onArgumentSaving;
  final Function toggleNavBar;
  final scoreArguments;
  final Function toggleStartState;
  final Function setTempPlayerScores;
  final tempPlayerScores;
  final Function funcKeepRallyCount;
  final int rallyCount;
  final bool player1Defalut;
  final bool player2Defalut;
  final Function funcSetplayerDefalut;
  final Function funcSetplayerServe;
  final int playerServe;
  final Function funcSetGameFinish;
  final Function onPageChanged;
  final Function funcSetGameTimeInSeconds;
  final int gameTimeInSeconds;
  final bool gameFinish;
  final Function togglePlayStateMain;
  final bool timerStatusMain;
  final int timeInSecondsMain;

  @override
  _Score createState() => _Score();

  Score({
    Key key,
    this.gameTimeInSeconds,
    this.funcSetGameTimeInSeconds,
    this.funcSetGameStart,
    this.gameFinish,
    this.funcSetGameFinish,
    this.playerServe,
    this.funcSetplayerServe,
    this.player1Defalut,
    this.player2Defalut,
    this.funcSetplayerDefalut,
    this.rallyCount,
    this.funcKeepRallyCount,
    this.tempPlayerScores,
    this.setTempPlayerScores,
    this.scoreArguments,
    this.toggleStartState,
    this.onSelected,
    this.onArgumentSaving,
    this.onPageChanged,
    this.toggleNavBar,
    this.togglePlayStateMain,
    this.timerStatusMain,
    this.timeInSecondsMain,
  }) : super(key: key);
}

class _Score extends State<Score>
    with SingleTickerProviderStateMixin, BaseControllerListner {
  AnimationController animationController;
  Animation<double> animation;
  BaseController controller;
  Widget playerOneImage;
  Widget playerTwoImage;
  Widget proGirl;
  Widget racket;
  Widget plus;
  Widget minus;
  Widget ball;
  Widget delete;

  Widget hrglass;
  Widget graph;
  Widget report;
  Widget settings;
  Widget undo;
  Widget play;

  //Global keys
  GlobalKey firstWinnerBtnKey;
  GlobalKey secondWinnerBtnKey;

  GlobalKey starMenuKey;

  GlobalKey firstUerrorBtnKey;
  GlobalKey secondUerrorBtnKey;

  GlobalKey firstFerrorBtnKey;
  GlobalKey secondFerrorBtnKey;

  int gameFinishSetMarginLine;

  var timerMainPage;
  int timeInSeconds = 0;
  bool semiAdvantageSecond = false;
  int semiAdvantageSecondInt = 0;

  @override
  void initState() {
    super.initState();

    controller = new BaseController(this);

    firstWinnerBtnKey = GlobalKey();
    secondWinnerBtnKey = GlobalKey();
    secondUerrorBtnKey = GlobalKey();
    firstUerrorBtnKey = GlobalKey();
    starMenuKey = GlobalKey();

    firstFerrorBtnKey = GlobalKey();
    secondFerrorBtnKey = GlobalKey();

    player1Id = widget.scoreArguments['player1Id'].toString();
    player2Id = widget.scoreArguments['player2Id'].toString();
    matchId = widget.scoreArguments['match_id'].toString();
    advantage = widget.scoreArguments['advantage'].toString();
    gameLimit = int.parse(widget.scoreArguments['noOfGames']);

    animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 5),
    )..addListener(() => setState(() {}));
    animation = Tween(begin: 0.0, end: 1.0).animate(animationController);

    if (widget.timerStatusMain == true) {
      timeInSeconds = widget.gameTimeInSeconds;
    } else {
      timeInSeconds = widget.timeInSecondsMain;
      togglePlayState();
    }

    timeFormatter();
    if (widget.scoreArguments['noOfSets'] != null) {
      setLimit = int.parse(widget.scoreArguments['noOfSets']);
    }

    if (widget.scoreArguments['tieBreak'] == ScoreValues.tieBreak) {
      tieBreak = true;
      breakCount = gameLimit;
      tieBrakerWinningLimit = 7;
    }

    if (widget.scoreArguments['tieBreak'] == ScoreValues.noTieBreak) {
      noTieBreak = true;
    }

    if (widget.scoreArguments['tieBreak'] == ScoreValues.superTieBreak) {
      superTieBreak = true;
      breakCount = gameLimit;
      tieBrakerWinningLimit = 10;
    }

    // IF GAME EXIST SET VALUES------------------------------------------------
    if (widget.tempPlayerScores.containsKey("player1serve")) {
      // print("------------------");
      // print(widget.tempPlayerScores['i1sAngle1Status']);
      // print(widget.tempPlayerScores['i1sAngle2Status']);
      // print(widget.tempPlayerScores['i1sAngle3Status']);
      // print(widget.tempPlayerScores['i1sAngle4Status']);
      // print(widget.tempPlayerScores['i1sAngle5Status']);
      // print(widget.tempPlayerScores['i2sAngle1Status']);
      // print(widget.tempPlayerScores['i2sAngle2Status']);
      // print(widget.tempPlayerScores['i2sAngle3Status']);
      // print(widget.tempPlayerScores['i2sAngle4Status']);
      // print(widget.tempPlayerScores['i2sAngle5Status']);

      // print(widget.tempPlayerScores);
      player1serve = widget.tempPlayerScores['player1serve'];
      player2serve = widget.tempPlayerScores['player2serve'];
      noOfTries = widget.rallyCount;
      if (widget.player1Defalut) {
        player1FaultError = ScoreValues.dflt;
      }
      if (widget.player2Defalut) {
        player2FaultError = ScoreValues.dflt;
      }

      if (widget.playerServe == 1) {
        player1serve = true;
        player2serve = false;
      } else {
        player1serve = false;
        player2serve = true;
      }

      if (widget.gameFinish) {
        gameFinish = true;
      } else {
        gameFinish = false;
      }

      player1noOfSets = widget.tempPlayerScores['player1noOfSets'];
      player1noOfGames = widget.tempPlayerScores['player1noOfGames'];
      player1noOfPoints = widget.tempPlayerScores['player1noOfPoints'];
      player2noOfSets = widget.tempPlayerScores['player2noOfSets'];
      player2noOfPoints = widget.tempPlayerScores['player2noOfPoints'];
      player2noOfGames = widget.tempPlayerScores['player2noOfGames'];
      totalSets = widget.tempPlayerScores['totalSets'];
      totalGames = widget.tempPlayerScores['totalGames'];
      player1inAd = widget.tempPlayerScores['player1inAd'];
      player2inAd = widget.tempPlayerScores['player2inAd'];
      player1noOfWonsInSet1 = widget.tempPlayerScores['player1noOfWonsInSet1'];
      player1noOfWonsInSet2 = widget.tempPlayerScores['player1noOfWonsInSet2'];
      player1noOfWonsInSet3 = widget.tempPlayerScores['player1noOfWonsInSet3'];
      player1noOfWonsInSet4 = widget.tempPlayerScores['player1noOfWonsInSet4'];
      player1noOfWonsInSet5 = widget.tempPlayerScores['player1noOfWonsInSet5'];
      player2noOfWonsInSet1 = widget.tempPlayerScores['player2noOfWonsInSet1'];
      player2noOfWonsInSet2 = widget.tempPlayerScores['player2noOfWonsInSet2'];
      player2noOfWonsInSet3 = widget.tempPlayerScores['player2noOfWonsInSet3'];
      player2noOfWonsInSet4 = widget.tempPlayerScores['player2noOfWonsInSet4'];
      player2noOfWonsInSet5 = widget.tempPlayerScores['player2noOfWonsInSet5'];
      i1sAngle1Status = widget.tempPlayerScores['i1sAngle1Status'];
      i1sAngle2Status = widget.tempPlayerScores['i1sAngle2Status'];
      i1sAngle3Status = widget.tempPlayerScores['i1sAngle3Status'];
      i1sAngle4Status = widget.tempPlayerScores['i1sAngle4Status'];
      i1sAngle5Status = widget.tempPlayerScores['i1sAngle5Status'];
      i2sAngle1Status = widget.tempPlayerScores['i2sAngle1Status'];
      i2sAngle2Status = widget.tempPlayerScores['i2sAngle2Status'];
      i2sAngle3Status = widget.tempPlayerScores['i2sAngle3Status'];
      i2sAngle4Status = widget.tempPlayerScores['i2sAngle4Status'];
      i2sAngle5Status = widget.tempPlayerScores['i2sAngle5Status'];

      trigerTieAngles = widget.tempPlayerScores['trigerTieAngles'];
      player1TieBreak1 = widget.tempPlayerScores['player1TieBreak1'];
      player1TieBreak2 = widget.tempPlayerScores['player1TieBreak2'];
      player1TieBreak3 = widget.tempPlayerScores['player1TieBreak3'];
      player1TieBreak4 = widget.tempPlayerScores['player1TieBreak4'];
      player1TieBreak5 = widget.tempPlayerScores['player1TieBreak5'];
      player2TieBreak1 = widget.tempPlayerScores['player2TieBreak1'];
      player2TieBreak2 = widget.tempPlayerScores['player2TieBreak2'];
      player2TieBreak3 = widget.tempPlayerScores['player2TieBreak3'];
      player2TieBreak4 = widget.tempPlayerScores['player2TieBreak4'];
      player2TieBreak5 = widget.tempPlayerScores['player2TieBreak5'];
      innerRadius = widget.tempPlayerScores['innerRadius'];
      x1tie1 = widget.tempPlayerScores['x1tie1'];
      x1tie2 = widget.tempPlayerScores['x1tie2'];
      x1tie3 = widget.tempPlayerScores['x1tie3'];
      x1tie4 = widget.tempPlayerScores['x1tie4'];
      x1tie5 = widget.tempPlayerScores['x1tie5'];
      x2tie1 = widget.tempPlayerScores['x2tie1'];
      x2tie2 = widget.tempPlayerScores['x2tie2'];
      x2tie3 = widget.tempPlayerScores['x2tie3'];
      x2tie4 = widget.tempPlayerScores['x2tie4'];
      x2tie5 = widget.tempPlayerScores['x2tie5'];
      breakProcessToTieBreak =
          widget.tempPlayerScores['breakProcessToTieBreak'];
      semiAdvantageSecond = widget.tempPlayerScores['semiAdvantageSecond'];
      // print(semiAdvantageSecond);
    }
    // IF GAME EXIST SET VALUES FINISHED------------------------------------------------

    getPlayer1Data();
    getPlayer2Data();

    racket = SvgPicture.asset('images/racket.svg');
    plus = SvgPicture.asset('images/s_plus.svg');
    ball = SvgPicture.asset('images/s_ball.svg');
    minus = SvgPicture.asset('images/s_minus.svg');
    hrglass = SvgPicture.asset('images/s_hrglass.svg');
    report = SvgPicture.asset('images/s_report.svg');
    graph = SvgPicture.asset('images/s_graph.svg');
    settings = SvgPicture.asset('images/s_settings.svg');
    undo = SvgPicture.asset('images/s_undo.svg');
    play = SvgPicture.asset('images/s_play.svg');
    delete = SvgPicture.asset('images/s_delete.svg');

    if (tieBreak == true) {
      innerRadius = 88.0;
    } else {
      innerRadius = 70.0;
    }
    innerDiameter = 2 * innerRadius;
    outerDiameter = 2 * outerRadius;

    checkEnabled();
  }

  int noOfTries = 1;
  bool noTieBreak = false;
  bool tieBreak = false;
  bool superTieBreak = false;
  int tieBrakerWinningLimit = 7;

  String player1No = '';
  String player2No = '';
  String player1Name = '';
  String player2Name = '';
  String weather = '';
  String stadium = '';
  String court = '';
  String advantage = '';
  var dateObject;
  var timeObject;

  double mainCardHeight = 300.0;
  double mainCardHeightSp = 300.0;
  var mainCardMargin = 15.0;
  double mainCardBorderWidth = 1.2;
  double mainCardBorderRadius = 5.0;
  var mainBackColor = AppColors.white;
  var mainCardBackColor = AppColors.secondary_color;
  var mainCardBorderColor = AppColors.primary_color;
  double leftContainerHeight = 220.0;
  double leftContainerHeightSp = 220.0;
  double rightContainerHeight = 220.0;

  double i1sAngle1 = 110.0;
  bool i1sAngle1Status = false;
  bool x1tie1 = false;

  double i1sAngle2 = 140.0;
  bool i1sAngle2Status = false;
  bool x1tie2 = false;

  double i1sAngle3 = 170.0;
  bool i1sAngle3Status = false;
  bool x1tie3 = false;

  double i1sAngle4 = 200.0;
  bool i1sAngle4Status = false;
  bool x1tie4 = false;

  double i1sAngle5 = 230.0;
  bool i1sAngle5Status = false;
  bool x1tie5 = false;

  //loku kotu layer 1
  double i1bAngle1 = -30.0;
  double i1bAngle2 = 15.0;
  double i1bAngle3 = 60.0;

  double i2sAngle1 = 110.0;
  bool i2sAngle1Status = false;
  bool x2tie1 = false;

  double i2sAngle2 = 140.0;
  bool i2sAngle2Status = false;
  bool x2tie2 = false;

  double i2sAngle3 = 170.0;
  bool i2sAngle3Status = false;
  bool x2tie3 = false;

  double i2sAngle4 = 200.0;
  bool i2sAngle4Status = false;
  bool x2tie4 = false;

  double i2sAngle5 = 230.0;
  bool i2sAngle5Status = false;
  bool x2tie5 = false;

  //loku kotu player 2
  double i2bAngle1 = -30.0;
  double i2bAngle2 = 15.0;
  double i2bAngle3 = 60.0;

  bool trigerTieAngles = false;
  int player1TieBreak1 = 0;
  int player1TieBreak2 = 0;
  int player1TieBreak3 = 0;
  int player1TieBreak4 = 0;
  int player1TieBreak5 = 0;
  int player2TieBreak1 = 0;
  int player2TieBreak2 = 0;
  int player2TieBreak3 = 0;
  int player2TieBreak4 = 0;
  int player2TieBreak5 = 0;
  int tempPTieBreak1 = 0;
  int tempPTieBreak2 = 0;

  // Shot type Initials
  double shortCircle = 60.0;
  double shortBox = 200;
  double shortCirclePadding = 2.0;

  // Small Box
  double smallBoxHeight = 18.0;
  var smallBoxColor = AppColors.primary_color;
  double smallBoxBorder = 1.0;
  double smallBoxBorderRadius = 4.0;
  var smallBorderColor = AppColors.black;
  double smallFontSize = 14;
  var smallFontWeight = FontWeight.w800;
  var smallFontColor = AppColors.white;

  // Tie Break box
  double smallFontSizeTie = 12;

  // Big Box
  double bigBoxHeight = 24.0;
  var bigBoxColor = AppColors.white;
  double bigBoxBorder = 1.2;
  double bigBoxBorderRadius = 4.0;
  var bigBorderColor = AppColors.black;
  double bigFontSize = 18;
  var bigFontWeight = FontWeight.w600;
  var bigFontColor = AppColors.black;
  double bigTitleFontSize = 10;
  var bigTitleFontWeight = FontWeight.w600;
  var bigTitleFontColor = AppColors.black;
  double bigMainBoxWidth = 30.0;
  double bigMainBoxHeight = 40.0;

  // Circle Values
  int player1noOfSets = 0;
  int player1noOfGames = 0;
  int player1noOfPoints = 0;
  int player1noOfWonsInSet1 = 0;
  int player1noOfWonsInSet2 = 0;
  int player1noOfWonsInSet3 = 0;
  int player1noOfWonsInSet4 = 0;
  int player1noOfWonsInSet5 = 0;
  int player2noOfSets = 0;
  int player2noOfGames = 0;
  int player2noOfPoints = 0;
  int player2noOfWonsInSet1 = 0;
  int player2noOfWonsInSet2 = 0;
  int player2noOfWonsInSet3 = 0;
  int player2noOfWonsInSet4 = 0;
  int player2noOfWonsInSet5 = 0;

  // Circles
  // double innerRadius = 72;
  double innerRadius = 70;
  double innerDiameter;

  double outerRadius = 90;
  double outerDiameter;

  // Left Side Action Buttons
  double actionBtnWidth = 180;
  double actionBtnHeight = 65.0;
  double miniActionBtnWidth = 70.0;
  double miniActionBtnHeight = 35.0;
  double miniActionBtnBorderRadius = 6.0;
  double miniActionBtnBorderWidth = 1.0;
  var miniActionBtnBorderColor = AppColors.black;
  double miniActionBtnFontSize = 16.0;
  var miniActionBtnFontWeight = FontWeight.w600;
  var miniActionBtnFontColor = AppColors.black;
  var miniActionBtnColor = AppColors.ternary_color;

  var actionButtonPadding = const EdgeInsets.only(left: 5.0);
  var actionButtonPadding2 = const EdgeInsets.only(left: 5.0, top: 8);
  double userErrorWidth = 100.0;
  double userErrorHeight = 80.0;
  double fErrorHeight = 70.0;
  var uErrorPadding = const EdgeInsets.only(top: 8.0);
  var acePadding = const EdgeInsets.only(top: 5, bottom: 0.0);
  double uerrorBtnFontSize = 18.0;
  var winnerButtonFontPadding = const EdgeInsets.only(right: 60.0);
  double aceButtonHeight = 50;
  double aceButtonWidth = 80;

  // Center Buttons
  double centerBtnheight = 40.0;
  double centerBtnWidth = 320.0;
  var centerMiniBtnColor = AppColors.primary_color;
  double centerMiniBtnBorderRadius = 6.0;
  double centerMiniBtnWidth = 156.0;
  double centerMiniBtnHeight = 40.0;
  var centerMiniBtnBorderColor = AppColors.black;
  double centerMiniBtnBorderWidth = 1.0;
  double centerMiniBtnFontSize = 16.0;
  var centerMiniBtnFontWeight = FontWeight.w900;
  var centerMiniBtnFontColor = AppColors.white;

  // Word Serve
  var wordServeFontColor = AppColors.primary_color;
  var wordServeFontWeight = FontWeight.w600;
  double wordServeFontSize = 14.0;

  // Name
  var nameFontColor = AppColors.black;
  var nameFontWeight = FontWeight.w600;
  double nameFontSize = 18.0;
  var nameMarginTop = 5.0;

  // Profile Image
  double profileImgWidth = 70.0;
  double profileImgHeight = 70.0;

  // Racket Image
  double racketImgWidth = 40.0;
  double racketImgHeight = 40.0;
  double racketImgCoverHeight = 40.0;
  double racketImgCoverWidth = 50.0;

  // OtherScreen Buttons
  double otherScreenBtnWidth = 320.0;
  double otherScreenBtnHeight = 50.0;
  var otherScreenBtnColor = AppColors.ternary_color;
  double otherScreenMiniBtnWidth = 60.0;
  double otherScreenMiniBtnHeight = 50.0;
  double otherScreenBorderWidth = 1.2;
  double otherScreenBorderRadius = 6.0;
  var otherScreenBorderColor = AppColors.black;
  double otherScreenImgHeight = 32.0;
  double otherScreenImgWidth = 32.0;

  bool player1serve = true;
  bool player2serve = false;

  bool subItem1Show = true;
  bool subItem2Show = true;
  bool player1inAd = false;
  bool player2inAd = false;

  String player1FaultError = ScoreValues.flt;
  String player2FaultError = ScoreValues.flt;

  int gameLimit = 6;
  int setLimit = 3;
  int totalSets = 0;
  int totalGames = 0;
  bool player1Pending = true;
  bool player2Pending = true;
  bool gameFinish = false;
  String matchId = '';
  String player1Id = '';
  String player2Id = '';
  String player1Type = "";
  String player2Type = "";
  int tempPlayer1noOfSets = 0;
  int tempPlayer1noOfGames = 0;
  int tempPlayer2noOfSets = 0;
  int tempPlayer2noOfGames = 0;
  int dbPlayerwonAgame = 0;
  int setChangeDetector = 0;
  bool undoBtn = false;
  int breakCount = 6;
  bool breakProcessToTieBreak = false;
  int tieBreakOn = 0;
  int firstOrSecondServe = 1;
  int firstOrSecondCounter = 0;

  //static var rallyCount = 0;

  static var aceDisabledBtn = AppColors.ternary_color;
  static var aceDisabledBtnFont = AppColors.white;

  static var playerImage1, playerImage2;

//updated to check the anable in ACE button-------------------
  void checkEnabled() {
    if ((noOfTries >= 2)) {
      setState(() {
        aceDisabledBtnFont = AppColors.black;
        aceDisabledBtn = AppColors.gray;
      });
    } else {
      setState(() {
        aceDisabledBtn = AppColors.ternary_color;
        aceDisabledBtnFont = AppColors.black;
      });
    }
  }

  //Check image availaibilty
  ImageProvider getImageProvider(File f) {
    return f.existsSync()
        ? FileImage(f)
        : const AssetImage("images/tennis-court.png");
  }

//---------------------------------------------------------------

  void updateScoreData(int x) {
    widget.funcSetGameStart(false);
    int rallyCount = noOfTries;
    int tempTieBreakOn = tieBreakOn;
    String won = '';
    int serve = 0;
    String type = '';

    if (x == 1) {
      won = player1Id;
      serve = player1serve ? 1 : 2;
      if (dbPlayerwonAgame == 1) {
        serve = player1serve ? 2 : 1;
      }

      type = player1Type;
    } else {
      won = player2Id;
      serve = player2serve ? 2 : 1;
      if (dbPlayerwonAgame == 1) {
        serve = player2serve ? 1 : 2;
      }
      type = player2Type;
    }

    if (semiAdvantageSecond == true) {
      semiAdvantageSecondInt = 1;
    } else {
      semiAdvantageSecondInt = 0;
    }

    setState(() {
      noOfTries = 1;
      tieBreakOn = 0;
    });
    widget.funcKeepRallyCount(1);

    var data = [
      matchId,
      won,
      type,
      serve,
      rallyCount,
      player1noOfSets,
      setChangeDetector == 1 ? tempPlayer1noOfGames : player1noOfGames,
      tempTieBreakOn == 1 ? tempPTieBreak1 : player1noOfPoints,
      player2noOfSets,
      setChangeDetector == 1 ? tempPlayer2noOfGames : player2noOfGames,
      tempTieBreakOn == 1 ? tempPTieBreak2 : player2noOfPoints,
      totalSets,
      totalGames,
      dbPlayerwonAgame,
      setChangeDetector,
      tempTieBreakOn,
      timeInSeconds,
      firstOrSecondServe,
      semiAdvantageSecondInt,
      breakProcessToTieBreak ? 1 : 0,
    ];

    var insertQuery = [
      "INSERT INTO scoreData (match_id, won, type, serve, rally, player1Sets, player1Games , player1Points, player2Sets, player2Games , player2Points, totalSets , totalGames , gameFinished , setFinished , tieBreakOn , gameTime, serveChance,semiAdvantageSecond,breakProcessToTieBreak) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
      data,
      {"calledMethod": "updateScoreData"}
    ];
    controller.execFunction(
      ControllerFunc.db_sqlite,
      ControllerSubFunc.db_insert,
      insertQuery,
    );
    setState(() {
      firstOrSecondServe = 1;
    });
  }

  void faultToDatabase(int x) {
    setState(() {
      undoBtn = true;
      firstOrSecondServe = 2;
    });
    int rallyCount = noOfTries;
    int tempTieBreakOn = tieBreakOn;
    String won = '';
    int serve = 0;
    String type = '';

    // print(player1serve);
    // print(player2serve);

    if (x == 1) {
      won = player1Id;
      serve = player1serve ? 1 : 2;
      // if (dbPlayerwonAgame == 1) {
      //   serve = player1serve ? 2 : 1;
      // }

      type = player2Type;
    } else {
      won = player2Id;
      serve = player2serve ? 2 : 1;
      // if (dbPlayerwonAgame == 1) {
      //   serve = player2serve ? 1 : 2;
      // }
      type = player1Type;
    }

    if (semiAdvantageSecond == true) {
      semiAdvantageSecondInt = 1;
    } else {
      semiAdvantageSecondInt = 0;
    }

    var data = [
      matchId,
      won,
      type,
      serve,
      rallyCount,
      player1noOfSets,
      setChangeDetector == 1 ? tempPlayer1noOfGames : player1noOfGames,
      tempTieBreakOn == 1 ? tempPTieBreak1 : player1noOfPoints,
      player2noOfSets,
      setChangeDetector == 1 ? tempPlayer2noOfGames : player2noOfGames,
      tempTieBreakOn == 1 ? tempPTieBreak2 : player2noOfPoints,
      totalSets,
      totalGames,
      dbPlayerwonAgame,
      setChangeDetector,
      tempTieBreakOn,
      timeInSeconds,
      1,
      semiAdvantageSecondInt,
      breakProcessToTieBreak ? 1 : 0,
    ];

    // print(data);

    var insertQuery = [
      "INSERT INTO scoreData (match_id, won, type, serve, rally, player1Sets, player1Games , player1Points, player2Sets, player2Games , player2Points, totalSets , totalGames , gameFinished , setFinished , tieBreakOn , gameTime, serveChance, semiAdvantageSecond , breakProcessToTieBreak) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
      data,
      {"calledMethod": "faultToDatabase"}
    ];
    controller.execFunction(
      ControllerFunc.db_sqlite,
      ControllerSubFunc.db_insert,
      insertQuery,
    );
  }

  void selectScoreData() {
    // print(matchId);
    var selectQuery = [
      "SELECT * FROM scoreData WHERE match_id=? ORDER BY id DESC LIMIT 2",
      [matchId],
      {"calledMethod": "selectScoreData"}
    ];
    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_select, selectQuery);
  }

  void setEndOfTheGame() {
    String winnerId = '';
    String looserId = '';
    int winnerSet = 0;
    int looserSet = 0;
    if (player1noOfSets > player2noOfSets) {
      winnerId = player1Id;
      looserId = player2Id;
      winnerSet = player1noOfSets;
      looserSet = player2noOfSets;
    } else {
      winnerId = player2Id;
      looserId = player1Id;
      winnerSet = player2noOfSets;
      looserSet = player1noOfSets;
    }
    var updateQuery = [
      "UPDATE result SET game_time=?, winner=? , looser=?, winner_set=? , looser_set=? , game_Status=? WHERE match_id=?",
      [timeInSeconds, winnerId, looserId, winnerSet, looserSet, 1, matchId],
      {"calledMethod": "setEndOfTheGame"}
    ];
    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_update, updateQuery);
  }

  void printMe() {
    var selectQuery = [
      "SELECT * FROM setInfo WHERE match_id=?",
      [matchId],
      {"calledMethod": "printMe"}
    ];
    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_select, selectQuery);
  }

  void getPlayer1Data() {
    var selectQuery = [
      "SELECT * FROM player WHERE player_id = ?",
      [player1Id],
      {"calledMethod": "getPlayer1Data"}
    ];
    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_select, selectQuery);
  }

  void getPlayer2Data() {
    var selectQuery = [
      "SELECT * FROM player WHERE player_id = ?",
      [player2Id],
      {"calledMethod": "getPlayer2Data"}
    ];
    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_select, selectQuery);
  }

  void deleteScoreData(int id) {
    var selectQuery = [
      "DELETE FROM scoreData WHERE id = ?",
      [id],
      {"calledMethod": "deleteScoreData"}
    ];
    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_delete, selectQuery);
  }

  void deleteAllData() {
    List queries = [];
    List params = [];

    queries.add("DELETE FROM scoreData WHERE match_id = ?");
    params.add([matchId]);
    queries.add("DELETE FROM gameSettings WHERE match_id = ?");
    params.add([matchId]);
    queries.add("DELETE FROM result WHERE match_id = ?");
    params.add([matchId]);

    var querywithParams = [
      queries,
      params,
      {"calledMethod": 'deleteAllData'}
    ];

    controller.execFunction(ControllerFunc.db_sqlite,
        ControllerSubFunc.db_select_batch, querywithParams);
    // var selectQuery = [
    //   "DELETE FROM scoreData WHERE match_id = ?",
    //   [matchId],
    //   {"calledMethod": "deleteAllData"}
    // ];
    // controller.execFunction(
    //     ControllerFunc.db_sqlite, ControllerSubFunc.db_delete, selectQuery);
  }

  // FUNCTIONS---------------------------------------------------------------
  degToRad(double deg) => deg * (pi / 180.0);
  // radToDeg(double rad) => rad * (180.0 / pi);

  getx(double val) {
    return -1 * cos(degToRad(val));
  }

  gety(double val) {
    return -1 * sin(degToRad(val));
  }

  void noOfTriesAdder() {
    setState(() {
      noOfTries++;
    });
    widget.funcKeepRallyCount(noOfTries);
  }

  void noOfTriesReducer() {
    if (noOfTries > 1) {
      setState(() {
        noOfTries--;
      });
      widget.funcKeepRallyCount(noOfTries);
    }
  }

  void gameIncrement(int x) {
    if (x == 1) {
      setState(() {
        totalGames++;
        player1noOfPoints = 0;
        player2noOfPoints = 0;
        player1serve = !player1serve;
        player2serve = !player2serve;
        if (player1serve) {
          widget.funcSetplayerServe(1);
        } else {
          widget.funcSetplayerServe(2);
        }

        player1inAd = false;
        player2inAd = false;
        dbPlayerwonAgame = 1;
      });
      if (player1noOfGames < gameLimit - 1) {
        setState(() {
          player1noOfGames++;
        });
      } else {
        if (player1noOfGames >= (player2noOfGames + 1)) {
          checkTotalSet(1);
        } else {
          setState(() {
            player1noOfGames++;
          });
          if (breakProcessToTieBreak) {
            setState(() {
              breakProcessToTieBreak = false;
            });
            tieBreakGameIncrement(1);
          }
          if (player1noOfGames == breakCount &&
              player2noOfGames == breakCount &&
              !noTieBreak) {
            setState(() {
              breakProcessToTieBreak = true;
            });
          }
        }
      }
    }

    // PLAYER 2 ---------------------------------
    else {
      setState(() {
        totalGames++;
        player2noOfPoints = 0;
        player1noOfPoints = 0;
        player1serve = !player1serve;
        player2serve = !player2serve;
        if (player1serve) {
          widget.funcSetplayerServe(1);
        } else {
          widget.funcSetplayerServe(2);
        }
        player2inAd = false;
        player1inAd = false;
        dbPlayerwonAgame = 1;
      });
      if (player2noOfGames < gameLimit - 1) {
        setState(() {
          player2noOfGames++;
        });
      } else {
        if (player2noOfGames >= (player1noOfGames + 1)) {
          checkTotalSet(2);
        } else {
          setState(() {
            player2noOfGames++;
          });
          if (breakProcessToTieBreak) {
            setState(() {
              breakProcessToTieBreak = false;
            });
            tieBreakGameIncrement(2);
          }
          if (player1noOfGames == breakCount &&
              player2noOfGames == breakCount &&
              !noTieBreak) {
            setState(() {
              breakProcessToTieBreak = true;
            });
          }
        }
      }
    }
  }

  void showupTieBreakPoints() {
    setState(() {
      trigerTieAngles = true;
      innerRadius = 88;
      innerDiameter = 2 * innerRadius;
    });
    var lastItem = 0;
    if (i1sAngle1Status) {
      lastItem = 1;
      if (i1sAngle2Status) {
        lastItem = 2;
        if (i1sAngle3Status) {
          lastItem = 3;
          if (i1sAngle4Status) {
            lastItem = 4;
            if (i1sAngle5Status) {
              lastItem = 5;
            }
          }
        }
      }
    }

    switch (lastItem) {
      case 0:
        break;
      case 1:
        setState(() {
          x1tie1 = true;
          x2tie1 = true;
        });
        break;
      case 2:
        setState(() {
          x1tie2 = true;
          x2tie2 = true;
        });
        break;
      case 3:
        setState(() {
          x1tie3 = true;
          x2tie3 = true;
        });
        break;
      case 4:
        setState(() {
          x1tie4 = true;
          x2tie4 = true;
        });
        break;
      case 5:
        setState(() {
          x1tie5 = true;
          x2tie5 = true;
        });
        break;
      default:
    }
  }

  void tieBreakGameIncrement(int x) {
    if (x == 1) {
      if (totalSets < setLimit) {
        setState(() {
          player1noOfSets++;
        });
        showSetTile();
        showupTieBreakPoints();
        setState(() {
          tempPlayer1noOfGames = player1noOfGames;
          tempPlayer2noOfGames = player2noOfGames;
          player2noOfGames = 0;
          player1noOfGames = 0;
        });
      } else {
        setState(() {
          player2Pending = false;
        });
      }
    } else {
      if (totalSets < setLimit) {
        setState(() {
          player2noOfSets++;
        });
        showSetTile();
        showupTieBreakPoints();
        setState(() {
          tempPlayer1noOfGames = player1noOfGames;
          tempPlayer2noOfGames = player2noOfGames;
          player2noOfGames = 0;
          player1noOfGames = 0;
        });
      } else {
        setState(() {
          player1Pending = false;
        });
      }
    }
  }

  void checkTotalSet(int x) {
    if (x == 1) {
      if (totalSets < setLimit) {
        setState(() {
          player1noOfGames++;
          player1noOfSets++;
        });
        showSetTile();
        setState(() {
          tempPlayer1noOfGames = player1noOfGames;
          tempPlayer2noOfGames = player2noOfGames;
          player2noOfGames = 0;
          player1noOfGames = 0;
        });
      } else {
        setState(() {
          player2Pending = false;
        });
      }
    } else {
      if (totalSets < setLimit) {
        setState(() {
          player2noOfGames++;
          player2noOfSets++;
        });
        showSetTile();
        setState(() {
          tempPlayer1noOfGames = player1noOfGames;
          tempPlayer2noOfGames = player2noOfGames;
          player2noOfGames = 0;
          player1noOfGames = 0;
        });
      } else {
        setState(() {
          player1Pending = false;
        });
      }
    }
  }

  // PLAYER1 INCREMENT -------------------------------------------------------------------
  void incrementPlayer1() {
    // print("incrementPlayer1");
    setState(() {
      undoBtn = true;
      player1FaultError = ScoreValues.flt;
      player2FaultError = ScoreValues.flt;
      widget.funcSetplayerDefalut(false, false);
    });
    if (player1Pending) {
      setState(() {
        dbPlayerwonAgame = 0;
        setChangeDetector = 0;
      });
      // BREAKING THE PROCESS---------------------------------------------
      if (breakProcessToTieBreak) {
        setState(() {
          player1inAd = false;
          player2inAd = false;
        });
        player1noOfPoints++;
        if (player1noOfPoints >= tieBrakerWinningLimit) {
          if (player1noOfPoints >= player2noOfPoints + 2) {
            setState(() {
              tieBreakOn = 1;
              tempPTieBreak1 = player1noOfPoints;
              tempPTieBreak2 = player2noOfPoints;
            });

            gameIncrement(1);
          }
        }
      } else {
        // NORMALIZE THE PROCESS---------------------------------------------
        switch (player1noOfPoints) {
          case 0:
            setState(() {
              player1noOfPoints = 15;
            });
            break;
          case 15:
            setState(() {
              player1noOfPoints = 30;
            });
            break;
          case 30:
            setState(() {
              player1noOfPoints = 40;
            });
            break;
          case 40:
            if (player2noOfPoints < 40) {
              gameIncrement(1);
            } else if (player2noOfPoints == 40) {
              if (advantage == ScoreValues.advantage) {
                setState(() {
                  player1noOfPoints = 41;
                  player1inAd = true;
                });
              } else if (advantage == ScoreValues.semiAdvantage) {
                if (semiAdvantageSecond) {
                  gameIncrement(1);
                  setState(() {
                    semiAdvantageSecond = false;
                  });
                } else {
                  setState(() {
                    player1noOfPoints = 41;
                    player1inAd = true;
                    semiAdvantageSecond = true;
                  });
                }
              } else {
                gameIncrement(1);
                break;
              }
            } else if (player2noOfPoints == 41) {
              if (advantage == ScoreValues.advantage) {
                setState(() {
                  player1noOfPoints = 40;
                  player2noOfPoints = 40;
                  player1inAd = false;
                  player2inAd = false;
                });
              } else if (advantage == ScoreValues.semiAdvantage) {
                setState(() {
                  player1noOfPoints = 40;
                  player2noOfPoints = 40;
                  player1inAd = false;
                  player2inAd = false;
                });
                // Semi advantage method changed
                // gameIncrement(1);
              }
            }
            break;
          case 41:
            gameIncrement(1);
            break;
          default:
            break;
        }
      }

      updateScoreData(1);
    }
  }

  // SHOW TILE -------------------------------------------------------------------
  void showSetTile() {
    setState(() {
      totalSets++;
      setChangeDetector = 1;
    });

    if (setLimit == 5) {
      gameFinishSetMarginLine = 3;
    } else if (setLimit == 3) {
      gameFinishSetMarginLine = 2;
    } else {
      gameFinishSetMarginLine = 1;
    }

    if ((player1noOfSets == gameFinishSetMarginLine) ||
        (player2noOfSets == gameFinishSetMarginLine)) {
      // if (totalSets == setLimit) {
      setState(() {
        player1Pending = false;
        player2Pending = false;
        _timer?.cancel();
        _timer = null;
        animationController.value = 1;
        gameFinish = true;
      });
      widget.funcSetGameFinish(true);
      setEndOfTheGame();
    }

    switch (totalSets) {
      case 1:
        setState(() {
          if (!i1sAngle1Status && !i2sAngle1Status) {
            i1sAngle1Status = true;
            player1noOfWonsInSet1 = player1noOfGames;
            i2sAngle1Status = true;
            player2noOfWonsInSet1 = player2noOfGames;
            player1TieBreak1 = tempPTieBreak1;
            player2TieBreak1 = tempPTieBreak2;
          }
        });
        break;

      case 2:
        setState(() {
          if (!i1sAngle2Status && !i2sAngle2Status) {
            i1sAngle2Status = true;
            player1noOfWonsInSet2 = player1noOfGames;
            i2sAngle2Status = true;
            player2noOfWonsInSet2 = player2noOfGames;
            player1TieBreak2 = tempPTieBreak1;
            player2TieBreak2 = tempPTieBreak2;
          }
        });
        break;

      case 3:
        setState(() {
          if (!i1sAngle3Status && !i2sAngle3Status) {
            i1sAngle3Status = true;
            player1noOfWonsInSet3 = player1noOfGames;
            i2sAngle3Status = true;
            player2noOfWonsInSet3 = player2noOfGames;
            player1TieBreak3 = tempPTieBreak1;
            player2TieBreak3 = tempPTieBreak2;
          }
        });
        break;

      case 4:
        setState(() {
          if (!i1sAngle4Status && !i2sAngle4Status) {
            i1sAngle4Status = true;
            player1noOfWonsInSet4 = player1noOfGames;
            i2sAngle4Status = true;
            player2noOfWonsInSet4 = player2noOfGames;
            player1TieBreak4 = tempPTieBreak1;
            player2TieBreak4 = tempPTieBreak2;
          }
        });
        break;

      case 5:
        setState(() {
          if (!i1sAngle5Status && !i2sAngle5Status) {
            i1sAngle5Status = true;
            player1noOfWonsInSet5 = player1noOfGames;
            i2sAngle5Status = true;
            player2noOfWonsInSet5 = player2noOfGames;
            player1TieBreak5 = tempPTieBreak1;
            player2TieBreak5 = tempPTieBreak2;
          }
        });
        break;
      default:
    }
  }

  // PLAYER2 INCREMENT -------------------------------------------------------------------
  void incrementPlayer2() {
    setState(() {
      undoBtn = true;
      player1FaultError = ScoreValues.flt;
      player2FaultError = ScoreValues.flt;
      widget.funcSetplayerDefalut(false, false);
    });
    if (player2Pending) {
      setState(() {
        dbPlayerwonAgame = 0;
        setChangeDetector = 0;
      });

      // BREAKING THE PROCESS---------------------------------------------
      if (breakProcessToTieBreak) {
        setState(() {
          player1inAd = false;
          player2inAd = false;
        });
        player2noOfPoints++;
        if (player2noOfPoints >= 7) {
          if (player2noOfPoints >= player1noOfPoints + 2) {
            setState(() {
              tieBreakOn = 1;
              tempPTieBreak1 = player1noOfPoints;
              tempPTieBreak2 = player2noOfPoints;
            });
            gameIncrement(2);
          }
        }
      } else {
        // NORMALIZE THE PROCESS---------------------------------------------
        switch (player2noOfPoints) {
          case 0:
            setState(() {
              player2noOfPoints = 15;
            });
            break;
          case 15:
            setState(() {
              player2noOfPoints = 30;
            });
            break;
          case 30:
            setState(() {
              player2noOfPoints = 40;
            });
            break;
          case 40:
            if (player1noOfPoints < 40) {
              gameIncrement(2);
            } else if (player1noOfPoints == 40) {
              if (advantage == ScoreValues.advantage) {
                setState(() {
                  player2noOfPoints = 41;
                  player2inAd = true;
                });
              } else if (advantage == ScoreValues.semiAdvantage) {
                if (semiAdvantageSecond) {
                  gameIncrement(2);
                  setState(() {
                    semiAdvantageSecond = false;
                  });
                } else {
                  setState(() {
                    player2noOfPoints = 41;
                    player2inAd = true;
                    semiAdvantageSecond = true;
                  });
                }
              } else {
                gameIncrement(2);
              }
            } else if (player1noOfPoints == 41) {
              if (advantage == ScoreValues.advantage) {
                setState(() {
                  player2noOfPoints = 40;
                  player1noOfPoints = 40;
                  player2inAd = false;
                  player1inAd = false;
                });
              } else if (advantage == ScoreValues.semiAdvantage) {
                setState(() {
                  player2noOfPoints = 40;
                  player1noOfPoints = 40;
                  player2inAd = false;
                  player1inAd = false;
                });
                // Semi advantage is changed
                // gameIncrement(2);
              }
            }
            break;
          case 41:
            gameIncrement(2);
            break;
          default:
            break;
        }
      }

      updateScoreData(2);
    }
  }

  // PLAYER1 ACE -------------------------------------------------------------------
  void incrementACEPlayer1() {
    if (noOfTries >= 2) {
    } else {
      setState(() {
        player1Type = ScoreValues.ace;
      });
      incrementPlayer1();
    }
  }

  // PLAYER2 ACE -------------------------------------------------------------------
  void incrementACEPlayer2() {
    if (noOfTries >= 2) {
    } else {
      setState(() {
        player2Type = ScoreValues.ace;
      });
      incrementPlayer2();
    }
  }

  void winnerTypeSetup(String type) {
    setState(() {
      player2Type = type;
      player1Type = type;
    });
  }

  // PLAYER1 FALULT -------------------------------------------------------------------
  void faultPlayer1() {
    if (noOfTries >= 2) {
    } else {
      if (player1FaultError == ScoreValues.dflt) {
        setState(() {
          // player1serve = false;
          // player2serve = true;
          player1FaultError = ScoreValues.flt;
          player2Type = ScoreValues.dflt;
        });

        incrementPlayer2();
      } else {
        setState(() {
          player1FaultError = ScoreValues.dflt;
          player2Type = ScoreValues.flt;
        });
        widget.funcSetplayerDefalut(true, false);
        faultToDatabase(1);
      }
    }
  }

  // PLAYER2 FALULT -------------------------------------------------------------------
  void faultPlayer2() {
    if (noOfTries >= 2) {
    } else {
      if (player2FaultError == ScoreValues.dflt) {
        setState(() {
          // player1serve = true;
          // player2serve = false;
          player2FaultError = ScoreValues.flt;
          player1Type = ScoreValues.dflt;
        });
        incrementPlayer1();
      } else {
        setState(() {
          player2FaultError = ScoreValues.dflt;
          player1Type = ScoreValues.flt;
        });
        widget.funcSetplayerDefalut(false, true);
        faultToDatabase(2);
      }
    }
  }

  // PLAYER U ERROR -------------------------------------------------------------------
  void uerrorPlayer2() {
    setState(() {
      player1Type = ScoreValues.uErr;
    });
    incrementPlayer1();
  }

  // PLAYER U ERROR -------------------------------------------------------------------
  void uerrorPlayer1() {
    setState(() {
      player2Type = ScoreValues.uErr;
    });
    incrementPlayer2();
  }

  // PLAYER U ERROR -------------------------------------------------------------------
  void ferrorPlayer2() {
    setState(() {
      player1Type = ScoreValues.fErr;
    });
    incrementPlayer1();
  }

  // PLAYER U ERROR -------------------------------------------------------------------
  void ferrorPlayer1() {
    setState(() {
      player2Type = ScoreValues.fErr;
    });
    incrementPlayer2();
  }

  String timeInNiceFormat = '00 : 00 : 00';

  void timeFormatter() {
    if (timeInSeconds == null) {
      timeInSeconds = 0;
    }

    int time = timeInSeconds.toInt();
    int secs = time % 60;
    int mins = ((time / 60) % 60).toInt();
    int hrs = time ~/ 3600;

    String finalTime = '';

    if (hrs < 10) {
      finalTime += '0$hrs : ';
    } else {
      finalTime += '$hrs : ';
    }

    if (mins < 10) {
      finalTime += '0$mins : ';
    } else {
      finalTime += '$mins : ';
    }

    if (secs < 10) {
      finalTime += '0$secs';
    } else {
      finalTime += '$secs';
    }

    setState(
      () {
        timeInNiceFormat = finalTime;
      },
    );
  }

  bool timerStatus = false;
  Timer _timer;
  final oneSec = Duration(seconds: 1);

  void startTimer() {}

  _setTimmerData() {
    var updateQuery = [
      "UPDATE result SET  game_tot_time=? WHERE match_id=?",
      [timeInSeconds, matchId],
      {"calledMethod": "setTimeData"}
    ];
    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_update, updateQuery);
  }

  void togglePlayState() {
    _setTimmerData();

    _timer?.cancel();
    _timer = null;

    if (Const.timeChange) {
      // Currently Playing but need to stop now

      play = SvgPicture.asset('images/s_play.svg');
      animationController.value = 1;
      setState(() {
        timerStatus = false;
      });
    } else {
      // Currently Stopped but need to run now
      _timer = Timer.periodic(
        oneSec,
        (Timer timer) => setState(
          () {
            timeInSeconds = widget.timeInSecondsMain;
            timeFormatter();
            play = SvgPicture.asset('images/s_pause.svg');
          },
        ),
      );

      play = SvgPicture.asset('images/s_pause.svg');

      animationController.repeat();
      setState(() {
        timerStatus = true;
      });
    }
  }

  timerPauseCallback(var params) {
    // Pause the game ------------------------------------
    if (params[0] == ScoreValues.pause) {
      resetItemsTemp();
      widget.toggleStartState(true);
      widget.onPageChanged(RoutingData.Home, true, false);

      _setTimmerData();
      widget.togglePlayStateMain(true, timeInSeconds, true, matchId);
      setState(() {
        Const.timeChange = true;
      });
      // Stop the game ------------------------------------
    } else if (params[0] == ScoreValues.stop) {
      resetItemsTemp();
      widget.toggleStartState(true);
      Navigator.pushReplacementNamed(context, "/MainPage");
      // Cancel the game ------------------------------------
    } else if (params[0] == ScoreValues.cancel) {
      if (params[1] == ScoreValues.pause) {}
    } else if (params[0] == ScoreValues.delete) {
      deleteAllData();
    }
  }

  setItemsTemp() {
    widget.setTempPlayerScores({
      'player1serve': player1serve,
      'player2serve': player2serve,
      'player1noOfSets': player1noOfSets,
      'player1noOfGames': player1noOfGames,
      'player1noOfPoints': player1noOfPoints,
      'player2noOfSets': player2noOfSets,
      'player2noOfPoints': player2noOfPoints,
      'player2noOfGames': player2noOfGames,
      'totalSets': totalSets,
      'totalGames': totalGames,
      'player1inAd': player1inAd,
      'player2inAd': player2inAd,
      'player1noOfWonsInSet1': player1noOfWonsInSet1,
      'player1noOfWonsInSet2': player1noOfWonsInSet2,
      'player1noOfWonsInSet3': player1noOfWonsInSet3,
      'player1noOfWonsInSet4': player1noOfWonsInSet4,
      'player1noOfWonsInSet5': player1noOfWonsInSet5,
      'player2noOfWonsInSet1': player2noOfWonsInSet1,
      'player2noOfWonsInSet2': player2noOfWonsInSet2,
      'player2noOfWonsInSet3': player2noOfWonsInSet3,
      'player2noOfWonsInSet4': player2noOfWonsInSet4,
      'player2noOfWonsInSet5': player2noOfWonsInSet5,
      'i1sAngle1Status': i1sAngle1Status,
      'i1sAngle2Status': i1sAngle2Status,
      'i1sAngle3Status': i1sAngle3Status,
      'i1sAngle4Status': i1sAngle4Status,
      'i1sAngle5Status': i1sAngle5Status,
      'i2sAngle1Status': i2sAngle1Status,
      'i2sAngle2Status': i2sAngle2Status,
      'i2sAngle3Status': i2sAngle3Status,
      'i2sAngle4Status': i2sAngle4Status,
      'i2sAngle5Status': i2sAngle5Status,
      'breakProcessToTieBreak': breakProcessToTieBreak,
      'weather': weather,
      'stadium': stadium,
      'court': court,
      'date': dateObject,
      'time': timeObject,
      'trigerTieAngles': trigerTieAngles,
      'player1TieBreak1': player1TieBreak1,
      'player1TieBreak2': player1TieBreak2,
      'player1TieBreak3': player1TieBreak3,
      'player1TieBreak4': player1TieBreak4,
      'player1TieBreak5': player1TieBreak5,
      'player2TieBreak1': player2TieBreak1,
      'player2TieBreak2': player2TieBreak2,
      'player2TieBreak3': player2TieBreak3,
      'player2TieBreak4': player2TieBreak4,
      'player2TieBreak5': player2TieBreak5,
      'innerRadius': innerRadius,
      'x1tie1': x1tie1,
      'x1tie2': x1tie2,
      'x1tie3': x1tie3,
      'x1tie4': x1tie4,
      'x1tie5': x1tie5,
      'x2tie1': x2tie1,
      'x2tie2': x2tie2,
      'x2tie3': x2tie3,
      'x2tie4': x2tie4,
      'x2tie5': x2tie5,
      'semiAdvantageSecond': semiAdvantageSecond
    });
  }

  resetItemsTemp() {
    widget.setTempPlayerScores({});
  }

  hideAllTiles() {
    i1sAngle1Status = false;
    i1sAngle2Status = false;
    i1sAngle3Status = false;
    i1sAngle4Status = false;
    i1sAngle5Status = false;
    i2sAngle1Status = false;
    i2sAngle2Status = false;
    i2sAngle3Status = false;
    i2sAngle4Status = false;
    i2sAngle5Status = false;
  }

  @override
  dispose() {
    _setTimmerData();

    _timer?.cancel();
    _timer = null;
    animationController.dispose();
    super.dispose();
  }

  customFontTopic(text) {
    return Text(
      text,
      textAlign: TextAlign.center,
      style: TextStyle(
          decorationColor: Colors.white,
          fontSize: 12,
          color: Colors.black,
          fontWeight: FontWeight.w600,
          fontFamily: 'Rajdhani'),
    );
  }

  Widget _buildMenu(
      GlobalKey parent, MenuShape shape, double startAngle, int player) {
    return StarMenu(
      key: starMenuKey,
      parentKey: parent,
      shape: shape,
      radiusX: 100,
      radiusY: 150,
      radiusIncrement: 5,
      startAngle: startAngle,
      endAngle: 360,
      durationMs: 1,
      rotateItemsAnimationAngle: 0.0,
      startItemScaleAnimation: 0.5,
      columns: 1,
      columnsSpaceH: 0,
      columnsSpaceV: 0,
      backgroundColor: Color.fromARGB(150, 0, 0, 0),
      checkScreenBoundaries: true,
      useScreenCenter: false,
      centerOffset: Offset(0, 0),
      animationCurve: Curves.easeIn,
      onItemPressed: (i) {
        // print("PRESSED $i");
      },
      items: <Widget>[
        Container(
          width: shortBox,
          height: shortBox,
          child: Stack(
            children: <Widget>[
              Align(
                alignment: Alignment(getx(90), gety(0)),
                child: GestureDetector(
                  onTap: () {
                    StarMenuState sms = starMenuKey.currentState;
                    sms.close();
                    checkEnabled();
                  },
                  child: Container(
                    width: shortCircle,
                    height: shortCircle,
                    child: ball,
                  ),
                ),
              ),
              Align(
                alignment: Alignment(getx(60), gety(60)),
                child: GestureDetector(
                  onTap: () {
                    setState(() {
                      if (player == 1 || player == 3) {
                        //set winner values
                        winnerTypeSetup(ScoreValues.backHand);
                      } else if (player == 5 || player == 6) {
                        //set f error values
                        winnerTypeSetup(ScoreValues.fErrBackHand);
                      } else {
                        //set u error vales
                        winnerTypeSetup(ScoreValues.uErrBackHand);
                      }

                      if (player == 1 || player == 4 || player == 6) {
                        incrementPlayer1();
                      } else {
                        incrementPlayer2();
                      }
                    });

                    StarMenuState sms = starMenuKey.currentState;
                    sms.close();

                    checkEnabled();
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white,
                    ),
                    width: shortCircle,
                    height: shortCircle,
                    child: Padding(
                      padding: EdgeInsets.all(shortCirclePadding),
                      child: Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                              color: Colors.black,
                              width: 1,
                            )),
                        child: Center(child: customFontTopic("BackHand")),
                      ),
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment(getx(120), gety(120)),
                child: GestureDetector(
                  onTap: () {
                    setState(() {
                      if (player == 1 || player == 3) {
                        //set winner values
                        winnerTypeSetup(ScoreValues.backHandVolley);
                      } else if (player == 5 || player == 6) {
                        //set f error
                        winnerTypeSetup(ScoreValues.fErrBackHandVolley);
                      } else {
                        //set u error vales
                        winnerTypeSetup(ScoreValues.uErrBackHandVolley);
                      }
                      if (player == 1 || player == 4 || player == 6) {
                        incrementPlayer1();
                      } else {
                        incrementPlayer2();
                      }
                    });
                    StarMenuState sms = starMenuKey.currentState;
                    sms.close();

                    checkEnabled();
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white,
                    ),
                    width: shortCircle,
                    height: shortCircle,
                    child: Padding(
                      padding: EdgeInsets.all(shortCirclePadding),
                      child: Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                              color: Colors.black,
                              width: 1,
                            )),
                        child:
                            Center(child: customFontTopic("BackHand Volley")),
                      ),
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment(getx(180), gety(180)),
                child: GestureDetector(
                  onTap: () {
                    setState(() {
                      if (player == 1 || player == 3) {
                        //set winner values
                        winnerTypeSetup(ScoreValues.foreHand);
                      } else if (player == 5 || player == 6) {
                        //set f error
                        winnerTypeSetup(ScoreValues.fErrForeHand);
                      } else {
                        //set user error vales
                        winnerTypeSetup(ScoreValues.uErrForeHand);
                      }

                      if (player == 1 || player == 4 || player == 6) {
                        incrementPlayer1();
                      } else {
                        incrementPlayer2();
                      }
                    });
                    StarMenuState sms = starMenuKey.currentState;
                    sms.close();

                    checkEnabled();
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white,
                    ),
                    width: shortCircle,
                    height: shortCircle,
                    child: Padding(
                      padding: EdgeInsets.all(shortCirclePadding),
                      child: Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                              color: Colors.black,
                              width: 1,
                            )),
                        child: Center(child: customFontTopic("ForeHand")),
                      ),
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment(getx(240), gety(240)),
                child: GestureDetector(
                  onTap: () {
                    setState(() {
                      if (player == 1 || player == 3) {
                        //set winner values
                        winnerTypeSetup(ScoreValues.forHandVolley);
                      } else if (player == 5 || player == 6) {
                        //set f error
                        winnerTypeSetup(ScoreValues.fErrForHandVolley);
                      } else {
                        //set user error vales
                        winnerTypeSetup(ScoreValues.uErrForHandVolley);
                      }

                      if (player == 1 || player == 4 || player == 6) {
                        incrementPlayer1();
                      } else {
                        incrementPlayer2();
                      }
                    });

                    StarMenuState sms = starMenuKey.currentState;
                    sms.close();

                    checkEnabled();
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white,
                    ),
                    width: shortCircle,
                    height: shortCircle,
                    child: Padding(
                      padding: EdgeInsets.all(shortCirclePadding),
                      child: Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                              color: Colors.black,
                              width: 1,
                            )),
                        child: Center(child: customFontTopic("ForHand Volley")),
                      ),
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment(getx(300), gety(300)),
                child: GestureDetector(
                  onTap: () {
                    setState(() {
                      if (player == 1 || player == 3) {
                        //set winner values
                        winnerTypeSetup(ScoreValues.smash);
                      } else if (player == 5 || player == 6) {
                        //set f error
                        winnerTypeSetup(ScoreValues.fErrSmash);
                      } else {
                        //set user error vales
                        winnerTypeSetup(ScoreValues.uErrSmash);
                      }
                      if (player == 1 || player == 4 || player == 6) {
                        incrementPlayer1();
                      } else {
                        incrementPlayer2();
                      }
                    });
                    StarMenuState sms = starMenuKey.currentState;
                    sms.close();

                    checkEnabled();
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white,
                    ),
                    width: shortCircle,
                    height: shortCircle,
                    child: Padding(
                      padding: EdgeInsets.all(shortCirclePadding),
                      child: Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                              color: Colors.black,
                              width: 1,
                            )),
                        child: Center(child: customFontTopic("Smash")),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  static var deviceWidth = 0.0;
  static var currentWidth = 0.0;
  static var deviceHeight = 0.0;
  static var currentHeight = 0.0;

  _setDeviceWidth() {
    if (deviceWidth <= 350) {
      setState(() {
        actionBtnWidth = 130;
        miniActionBtnWidth = 45;
        userErrorWidth = 75;
        aceButtonWidth = 60;
        uerrorBtnFontSize = 16.0;
        actionBtnHeight = 55;

        currentWidth = 300;
      });
    } else if (deviceWidth <= 400) {
      actionBtnWidth = 150;
      miniActionBtnWidth = 50;
      userErrorWidth = 90;
      aceButtonWidth = 70;
    } else if (deviceWidth <= 450) {
      actionBtnWidth = 180;
      miniActionBtnWidth = 70;
      userErrorWidth = 100;
      aceButtonWidth = 80;
    } else if (deviceWidth <= 550) {
      actionBtnWidth = 180;
      miniActionBtnWidth = 70;
      userErrorWidth = 100;
      aceButtonWidth = 80;
    } else if (deviceWidth > 550) {
      actionBtnWidth = 180;
      miniActionBtnWidth = 70;
      userErrorWidth = 100;
      aceButtonWidth = 80;

      actionBtnWidth = 230;
      miniActionBtnWidth = 100;
      userErrorWidth = 120;
      aceButtonWidth = 110;
    } else {}

    if (deviceHeight <= 500) {
    } else if ((deviceHeight <= 600)) {
    } else if ((deviceHeight <= 700)) {
    } else if ((deviceHeight <= 900)) {
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      deviceWidth = MediaQuery.of(context).size.width;
      deviceHeight = MediaQuery.of(context).size.height;
    });
    _setDeviceWidth();

    // MAIN RETURN ---------------------------------------------------------------
    return WillPopScope(
      child: Scaffold(
        body: MediaQuery(
          data: MediaQueryData(),
          child: Container(
              color: mainBackColor,
              child: ListView(children: <Widget>[
                AnimatedContainer(
                  height: player1serve ? mainCardHeight : mainCardHeightSp,
                  duration: Duration(milliseconds: 50),
                  child: Container(
                    margin: EdgeInsets.all(mainCardMargin),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(mainCardBorderRadius),
                      color: mainCardBackColor,
                      border: Border.all(
                        width: mainCardBorderWidth,
                        color: mainCardBorderColor,
                      ),
                    ),
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 30.0, right: 30),
                          child: Container(
                            margin: EdgeInsets.only(top: 0),
                            child: GestureDetector(
                              onTap: () {
                                // widget.toggleNavBar();
                                // selectScoreData();
                              },
                              child: Text(
                                player1No + " - " + player1Name,
                                textAlign: TextAlign.center,
                                overflow: TextOverflow.fade,
                                maxLines: 1,
                                softWrap: false,
                                style: TextStyle(
                                    height: 0.9,
                                    fontSize: nameFontSize,
                                    color: nameFontColor,
                                    fontWeight: nameFontWeight),
                              ),
                            ),
                          ),
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          // mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            //left hand side
                            Expanded(
                              child: AnimatedContainer(
                                  duration: Duration(milliseconds: 50),
                                  height: player1serve
                                      ? leftContainerHeight
                                      : leftContainerHeightSp,
                                  width: double.infinity,
                                  child: Column(
                                    children: <Widget>[
                                      Expanded(
                                        //player one serve diplay
                                        child: Container(
                                          width: racketImgCoverWidth,
                                          height: racketImgCoverHeight,
                                          child: Stack(
                                            children: <Widget>[
                                              GestureDetector(
                                                onTap: () {
                                                  setState(() {
                                                    player2serve = false;
                                                    player1serve = true;
                                                    player1FaultError =
                                                        ScoreValues.flt;
                                                    player2FaultError =
                                                        ScoreValues.flt;
                                                  });
                                                  widget.funcSetplayerServe(1);
                                                  checkEnabled();
                                                },
                                                child: Container(
                                                  width: racketImgWidth,
                                                  height: racketImgHeight,
                                                  decoration: BoxDecoration(),
                                                  child: racket,
                                                ),
                                              ),
                                              player1serve
                                                  ? Positioned(
                                                      right: 0,
                                                      bottom: 0,
                                                      child: Text(
                                                        'Serve',
                                                        style: TextStyle(
                                                          fontSize:
                                                              wordServeFontSize,
                                                          fontWeight:
                                                              wordServeFontWeight,
                                                          color:
                                                              wordServeFontColor,
                                                        ),
                                                      ),
                                                    )
                                                  : Container(),
                                            ],
                                          ),
                                        ),
                                      ),
                                      // player1serve
                                      //     ? Flexible(
                                      //         child: Align(
                                      //           child: Padding(
                                      //             padding: actionButtonPadding2,
                                      //             child: Container(
                                      //               height: aceButtonHeight,
                                      //               width: actionBtnWidth,
                                      //               alignment: Alignment.center,
                                      //               child: Row(
                                      //                 crossAxisAlignment:
                                      //                     CrossAxisAlignment
                                      //                         .center,
                                      //                 mainAxisSize:
                                      //                     MainAxisSize.max,
                                      //                 mainAxisAlignment:
                                      //                     MainAxisAlignment
                                      //                         .spaceBetween,
                                      //                 children: <Widget>[
                                      //                   //player one ACE button
                                      //                   Padding(
                                      //                     padding: acePadding,
                                      //                     child: Container(
                                      //                       width:
                                      //                           aceButtonWidth,
                                      //                       height:
                                      //                           userErrorHeight,
                                      //                       child: RaisedButton(
                                      //                         padding:
                                      //                             EdgeInsets
                                      //                                 .all(0),
                                      //                         shape: new RoundedRectangleBorder(
                                      //                             borderRadius:
                                      //                                 new BorderRadius
                                      //                                         .circular(
                                      //                                     miniActionBtnBorderRadius),
                                      //                             side: BorderSide(
                                      //                                 color:
                                      //                                     miniActionBtnBorderColor,
                                      //                                 width:
                                      //                                     miniActionBtnBorderWidth,
                                      //                                 style: BorderStyle
                                      //                                     .solid)),
                                      //                         onPressed: () {
                                      //                           incrementACEPlayer1();
                                      //                           checkEnabled();
                                      //                         },
                                      //                         color:
                                      //                             aceDisabledBtn,
                                      //                         child: Text(
                                      //                           'ACE',
                                      //                           style:
                                      //                               TextStyle(
                                      //                             fontSize:
                                      //                                 uerrorBtnFontSize,
                                      //                             fontWeight:
                                      //                                 miniActionBtnFontWeight,
                                      //                             color:
                                      //                                 miniActionBtnFontColor,
                                      //                           ),
                                      //                         ),
                                      //                       ),
                                      //                     ),
                                      //                   ),

                                      //                   //player one fault d fault display
                                      //                   Padding(
                                      //                     padding: acePadding,
                                      //                     child: Container(
                                      //                       width:
                                      //                           aceButtonWidth,
                                      //                       height:
                                      //                           userErrorHeight,
                                      //                       child: RaisedButton(
                                      //                         padding:
                                      //                             EdgeInsets
                                      //                                 .all(0),
                                      //                         shape: new RoundedRectangleBorder(
                                      //                             borderRadius:
                                      //                                 new BorderRadius
                                      //                                         .circular(
                                      //                                     miniActionBtnBorderRadius),
                                      //                             side: BorderSide(
                                      //                                 color:
                                      //                                     miniActionBtnBorderColor,
                                      //                                 width:
                                      //                                     miniActionBtnBorderWidth,
                                      //                                 style: BorderStyle
                                      //                                     .solid)),
                                      //                         onPressed: () {
                                      //                           faultPlayer1();
                                      //                           checkEnabled();
                                      //                         },
                                      //                         color:
                                      //                             aceDisabledBtn,
                                      //                         child: Text(
                                      //                           player1FaultError,
                                      //                           style:
                                      //                               TextStyle(
                                      //                             fontSize:
                                      //                                 uerrorBtnFontSize,
                                      //                             fontWeight:
                                      //                                 miniActionBtnFontWeight,
                                      //                             color:
                                      //                                 aceDisabledBtnFont,
                                      //                           ),
                                      //                         ),
                                      //                       ),
                                      //                     ),
                                      //                   ),
                                      //                 ],
                                      //               ),
                                      //             ),
                                      //           ),
                                      //         ),
                                      //       )
                                      //     : Flexible(
                                      //         child: Align(
                                      //           child: Padding(
                                      //             padding: actionButtonPadding2,
                                      //             child: Container(
                                      //               height: aceButtonHeight,
                                      //             ),
                                      //           ),
                                      //         ),
                                      //       ),
                                      //winner button player one
                                      Flexible(
                                        child: Align(
                                          child: Padding(
                                            padding: actionButtonPadding2,
                                            child: Container(
                                              height: actionBtnHeight,
                                              width: actionBtnWidth,
                                              alignment: Alignment.center,
                                              child: Container(
                                                width: double.infinity,
                                                child: RaisedButton(
                                                  padding: EdgeInsets.all(0),
                                                  shape: new RoundedRectangleBorder(
                                                      borderRadius: new BorderRadius
                                                              .circular(
                                                          miniActionBtnBorderRadius),
                                                      side: BorderSide(
                                                          color:
                                                              miniActionBtnBorderColor,
                                                          width:
                                                              miniActionBtnBorderWidth,
                                                          style: BorderStyle
                                                              .solid)),
                                                  onPressed: () {
                                                    winnerTypeSetup(
                                                        ScoreValues.winner);
                                                    incrementPlayer1();
                                                    checkEnabled();
                                                  },
                                                  color: miniActionBtnColor,
                                                  child: Stack(
                                                    children: <Widget>[
                                                      Container(
                                                        height: userErrorHeight,
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  right: 0.0),
                                                          child: Align(
                                                            alignment: Alignment
                                                                .centerRight,
                                                            child:
                                                                GestureDetector(
                                                              onTap: () {
                                                                StarMenuController
                                                                    .displayStarMenu(
                                                                        _buildMenu(
                                                                          firstWinnerBtnKey,
                                                                          MenuShape
                                                                              .grid,
                                                                          0.0,
                                                                          1,
                                                                        ),
                                                                        firstWinnerBtnKey);

                                                                checkEnabled();
                                                              },
                                                              child: Container(
                                                                //color: Colors.green,
                                                                //Global key
                                                                key:
                                                                    firstWinnerBtnKey,
                                                                width:
                                                                    actionBtnHeight,
                                                                height:
                                                                    actionBtnHeight,
                                                                child: Align(
                                                                  alignment:
                                                                      Alignment
                                                                          .center,
                                                                  child:
                                                                      Container(
                                                                    width: .6 *
                                                                        actionBtnHeight,
                                                                    height: .6 *
                                                                        actionBtnHeight,
                                                                    child: ball,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            winnerButtonFontPadding,
                                                        child: Align(
                                                          alignment:
                                                              Alignment.center,
                                                          child: Text(
                                                            'WINNER',
                                                            style: TextStyle(
                                                              fontSize:
                                                                  uerrorBtnFontSize,
                                                              fontWeight:
                                                                  miniActionBtnFontWeight,
                                                              color:
                                                                  miniActionBtnFontColor,
                                                            ),
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),

                                      //u.err button player one
                                      Flexible(
                                        child: Align(
                                          child: Padding(
                                            padding: actionButtonPadding,
                                            child: Container(
                                              height: actionBtnHeight,
                                              width: actionBtnWidth,
                                              alignment: Alignment.center,
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                mainAxisSize: MainAxisSize.max,
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Padding(
                                                    padding: uErrorPadding,
                                                    child: Container(
                                                      width: actionBtnWidth,
                                                      height: userErrorHeight,
                                                      child: RaisedButton(
                                                        padding:
                                                            EdgeInsets.all(0),
                                                        shape: new RoundedRectangleBorder(
                                                            borderRadius:
                                                                new BorderRadius
                                                                        .circular(
                                                                    miniActionBtnBorderRadius),
                                                            side: BorderSide(
                                                                color:
                                                                    miniActionBtnBorderColor,
                                                                width:
                                                                    miniActionBtnBorderWidth,
                                                                style:
                                                                    BorderStyle
                                                                        .solid)),
                                                        onPressed: () {
                                                          uerrorPlayer1();
                                                          checkEnabled();
                                                        },
                                                        color:
                                                            miniActionBtnColor,
                                                        child: Stack(
                                                          children: <Widget>[
                                                            Container(
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        right:
                                                                            0.0),
                                                                child: Align(
                                                                  alignment:
                                                                      Alignment
                                                                          .centerRight,
                                                                  child:
                                                                      GestureDetector(
                                                                    onTap: () {
                                                                      StarMenuController.displayStarMenu(
                                                                          _buildMenu(
                                                                            firstUerrorBtnKey,
                                                                            MenuShape.grid,
                                                                            0.0,
                                                                            2,
                                                                          ),
                                                                          firstUerrorBtnKey);

                                                                      checkEnabled();
                                                                    },
                                                                    child:
                                                                        Container(
                                                                      //color: Colors.green,
                                                                      key:
                                                                          firstUerrorBtnKey,
                                                                      width:
                                                                          actionBtnHeight,
                                                                      height:
                                                                          actionBtnHeight,
                                                                      child:
                                                                          Align(
                                                                        alignment:
                                                                            Alignment.center,
                                                                        child:
                                                                            Container(
                                                                          width:
                                                                              .6 * actionBtnHeight,
                                                                          height:
                                                                              .6 * actionBtnHeight,
                                                                          child:
                                                                              ball,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                            Padding(
                                                              padding:
                                                                  winnerButtonFontPadding,
                                                              child: Align(
                                                                alignment:
                                                                    Alignment
                                                                        .center,
                                                                child: Text(
                                                                  'U.ERR',
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        uerrorBtnFontSize,
                                                                    fontWeight:
                                                                        miniActionBtnFontWeight,
                                                                    color:
                                                                        miniActionBtnFontColor,
                                                                  ),
                                                                ),
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),

                                      //f .err player one
                                      Flexible(
                                        child: Align(
                                          child: Padding(
                                            padding: actionButtonPadding,
                                            child: Container(
                                              height: actionBtnHeight,
                                              width: actionBtnWidth,
                                              alignment: Alignment.center,
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                mainAxisSize: MainAxisSize.max,
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Padding(
                                                    padding: uErrorPadding,
                                                    child: Container(
                                                      width: actionBtnWidth,
                                                      height: userErrorHeight,
                                                      child: RaisedButton(
                                                        padding:
                                                            EdgeInsets.all(0),
                                                        shape: new RoundedRectangleBorder(
                                                            borderRadius:
                                                                new BorderRadius
                                                                        .circular(
                                                                    miniActionBtnBorderRadius),
                                                            side: BorderSide(
                                                                color:
                                                                    miniActionBtnBorderColor,
                                                                width:
                                                                    miniActionBtnBorderWidth,
                                                                style:
                                                                    BorderStyle
                                                                        .solid)),
                                                        onPressed: () {
                                                          ferrorPlayer1();
                                                          checkEnabled();
                                                        },
                                                        color:
                                                            miniActionBtnColor,
                                                        child: Stack(
                                                          children: <Widget>[
                                                            Container(
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        right:
                                                                            0.0),
                                                                child: Align(
                                                                  alignment:
                                                                      Alignment
                                                                          .centerRight,
                                                                  child:
                                                                      GestureDetector(
                                                                    onTap: () {
                                                                      // ferrorPlayer2();
                                                                      // checkEnabled();
                                                                      StarMenuController.displayStarMenu(
                                                                          _buildMenu(
                                                                            firstFerrorBtnKey,
                                                                            MenuShape.grid,
                                                                            0.0,
                                                                            5,
                                                                          ),
                                                                          firstFerrorBtnKey);
                                                                      checkEnabled();
                                                                    },
                                                                    child:
                                                                        Container(
                                                                      //color: Colors.red,
                                                                      key:
                                                                          firstFerrorBtnKey,
                                                                      width:
                                                                          actionBtnHeight,
                                                                      height:
                                                                          actionBtnHeight,
                                                                      child:
                                                                          Align(
                                                                        alignment:
                                                                            Alignment.center,
                                                                        child:
                                                                            Container(
                                                                          width:
                                                                              .6 * actionBtnHeight,
                                                                          height:
                                                                              .6 * actionBtnHeight,
                                                                          child:
                                                                              ball,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                            Padding(
                                                              padding:
                                                                  winnerButtonFontPadding,
                                                              child: Align(
                                                                alignment:
                                                                    Alignment
                                                                        .center,
                                                                child: Text(
                                                                  'F.ERR',
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        uerrorBtnFontSize,
                                                                    fontWeight:
                                                                        miniActionBtnFontWeight,
                                                                    color:
                                                                        miniActionBtnFontColor,
                                                                  ),
                                                                ),
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  )),
                            ),

                            // Right Side Block -------------------------------------
                            Expanded(
                              child: Container(
                                // color: Colors.green,
                                height: rightContainerHeight,
                                child: Stack(
                                  children: <Widget>[
                                    // Bigger Circle -------------------------------------
                                    Align(
                                        alignment: Alignment.center,
                                        child: Container(
                                          width: outerDiameter,
                                          height: outerDiameter,
                                          child: Stack(
                                            children: <Widget>[
                                              // no of games -------------------------------------
                                              subItem1Show
                                                  ? Align(
                                                      alignment:
                                                          Alignment(0, 0),
                                                      child: Container(
                                                        // color: Colors.red,
                                                        height: innerDiameter,
                                                        width: innerDiameter,
                                                        child: Stack(
                                                          children: <Widget>[
                                                            // 1 item -------------------------------------
                                                            AnimatedAlign(
                                                              duration: Duration(
                                                                  milliseconds:
                                                                      500),
                                                              alignment: Alignment(
                                                                  i1sAngle1Status
                                                                      ? getx(
                                                                          i1sAngle1)
                                                                      : 0,
                                                                  i1sAngle1Status
                                                                      ? gety(
                                                                          i1sAngle1)
                                                                      : 0),
                                                              child: Stack(
                                                                children: <
                                                                    Widget>[
                                                                  Container(
                                                                    height:
                                                                        smallBoxHeight,
                                                                    width:
                                                                        smallBoxHeight,
                                                                    alignment:
                                                                        Alignment
                                                                            .center,
                                                                    decoration:
                                                                        new BoxDecoration(
                                                                      color:
                                                                          smallBoxColor,
                                                                      border:
                                                                          new Border
                                                                              .all(
                                                                        width:
                                                                            smallBoxBorder,
                                                                        color:
                                                                            smallBorderColor,
                                                                      ),
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              smallBoxBorderRadius),
                                                                    ),
                                                                    child: Text(
                                                                      player1noOfWonsInSet1
                                                                          .toString(),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .center,
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            smallFontSize,
                                                                        color:
                                                                            smallFontColor,
                                                                        fontWeight:
                                                                            smallFontWeight,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  x1tie1
                                                                      ? Container(
                                                                          height:
                                                                              smallBoxHeight,
                                                                          width:
                                                                              2.2 * smallBoxHeight,
                                                                          child:
                                                                              Text(
                                                                            '($player1TieBreak1)',
                                                                            textAlign:
                                                                                TextAlign.right,
                                                                            style:
                                                                                TextStyle(
                                                                              fontSize: smallFontSizeTie,
                                                                              color: AppColors.black,
                                                                              fontWeight: smallFontWeight,
                                                                            ),
                                                                          ),
                                                                        )
                                                                      : trigerTieAngles
                                                                          ? Container(
                                                                              height: smallBoxHeight,
                                                                              width: 2.2 * smallBoxHeight,
                                                                            )
                                                                          : Container(
                                                                              height: smallBoxHeight,
                                                                              width: smallBoxHeight,
                                                                            ),
                                                                ],
                                                              ),
                                                            ),
                                                            // 2 item -------------------------------------
                                                            AnimatedAlign(
                                                              duration: Duration(
                                                                  milliseconds:
                                                                      500),
                                                              alignment: Alignment(
                                                                  i1sAngle2Status
                                                                      ? getx(
                                                                          i1sAngle2)
                                                                      : 0,
                                                                  i1sAngle2Status
                                                                      ? gety(
                                                                          i1sAngle2)
                                                                      : 0),
                                                              child: Stack(
                                                                children: <
                                                                    Widget>[
                                                                  Container(
                                                                    height:
                                                                        smallBoxHeight,
                                                                    width:
                                                                        smallBoxHeight,
                                                                    alignment:
                                                                        Alignment
                                                                            .center,
                                                                    decoration:
                                                                        new BoxDecoration(
                                                                      color:
                                                                          smallBoxColor,
                                                                      border:
                                                                          new Border
                                                                              .all(
                                                                        width:
                                                                            smallBoxBorder,
                                                                        color:
                                                                            smallBorderColor,
                                                                      ),
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              smallBoxBorderRadius),
                                                                    ),
                                                                    child: Text(
                                                                      player1noOfWonsInSet2
                                                                          .toString(),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .center,
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            smallFontSize,
                                                                        color:
                                                                            smallFontColor,
                                                                        fontWeight:
                                                                            smallFontWeight,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  x1tie2
                                                                      ? Container(
                                                                          height:
                                                                              smallBoxHeight,
                                                                          width:
                                                                              2.2 * smallBoxHeight,
                                                                          child:
                                                                              Text(
                                                                            '($player1TieBreak2)',
                                                                            textAlign:
                                                                                TextAlign.right,
                                                                            style:
                                                                                TextStyle(
                                                                              fontSize: smallFontSizeTie,
                                                                              color: AppColors.black,
                                                                              fontWeight: smallFontWeight,
                                                                            ),
                                                                          ),
                                                                        )
                                                                      : trigerTieAngles
                                                                          ? Container(
                                                                              height: smallBoxHeight,
                                                                              width: 2.2 * smallBoxHeight,
                                                                            )
                                                                          : Container(
                                                                              height: smallBoxHeight,
                                                                              width: smallBoxHeight,
                                                                            ),
                                                                ],
                                                              ),
                                                            ),
                                                            // 3 item -------------------------------------
                                                            AnimatedAlign(
                                                              duration: Duration(
                                                                  milliseconds:
                                                                      500),
                                                              alignment: Alignment(
                                                                  i1sAngle3Status
                                                                      ? getx(
                                                                          i1sAngle3)
                                                                      : 0,
                                                                  i1sAngle3Status
                                                                      ? gety(
                                                                          i1sAngle3)
                                                                      : 0),
                                                              child: Stack(
                                                                children: <
                                                                    Widget>[
                                                                  Container(
                                                                    height:
                                                                        smallBoxHeight,
                                                                    width:
                                                                        smallBoxHeight,
                                                                    alignment:
                                                                        Alignment
                                                                            .center,
                                                                    decoration:
                                                                        new BoxDecoration(
                                                                      color:
                                                                          smallBoxColor,
                                                                      border:
                                                                          new Border
                                                                              .all(
                                                                        width:
                                                                            smallBoxBorder,
                                                                        color:
                                                                            smallBorderColor,
                                                                      ),
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              smallBoxBorderRadius),
                                                                    ),
                                                                    child: Text(
                                                                      player1noOfWonsInSet3
                                                                          .toString(),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .center,
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            smallFontSize,
                                                                        color:
                                                                            smallFontColor,
                                                                        fontWeight:
                                                                            smallFontWeight,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  x1tie3
                                                                      ? Container(
                                                                          height:
                                                                              smallBoxHeight,
                                                                          width:
                                                                              2.2 * smallBoxHeight,
                                                                          child:
                                                                              Text(
                                                                            '($player1TieBreak3)',
                                                                            textAlign:
                                                                                TextAlign.right,
                                                                            style:
                                                                                TextStyle(
                                                                              fontSize: smallFontSizeTie,
                                                                              color: AppColors.black,
                                                                              fontWeight: smallFontWeight,
                                                                            ),
                                                                          ),
                                                                        )
                                                                      : trigerTieAngles
                                                                          ? Container(
                                                                              height: smallBoxHeight,
                                                                              width: 2.2 * smallBoxHeight,
                                                                            )
                                                                          : Container(
                                                                              height: smallBoxHeight,
                                                                              width: smallBoxHeight,
                                                                            ),
                                                                ],
                                                              ),
                                                            ),
                                                            // 4 item -------------------------------------
                                                            AnimatedAlign(
                                                              duration: Duration(
                                                                  milliseconds:
                                                                      500),
                                                              alignment: Alignment(
                                                                  i1sAngle4Status
                                                                      ? getx(
                                                                          i1sAngle4)
                                                                      : 0,
                                                                  i1sAngle4Status
                                                                      ? gety(
                                                                          i1sAngle4)
                                                                      : 0),
                                                              child: Stack(
                                                                children: <
                                                                    Widget>[
                                                                  Container(
                                                                    height:
                                                                        smallBoxHeight,
                                                                    width:
                                                                        smallBoxHeight,
                                                                    alignment:
                                                                        Alignment
                                                                            .center,
                                                                    decoration:
                                                                        new BoxDecoration(
                                                                      color:
                                                                          smallBoxColor,
                                                                      border:
                                                                          new Border
                                                                              .all(
                                                                        width:
                                                                            smallBoxBorder,
                                                                        color:
                                                                            smallBorderColor,
                                                                      ),
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              smallBoxBorderRadius),
                                                                    ),
                                                                    child: Text(
                                                                      player1noOfWonsInSet4
                                                                          .toString(),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .center,
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            smallFontSize,
                                                                        color:
                                                                            smallFontColor,
                                                                        fontWeight:
                                                                            smallFontWeight,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  x1tie4
                                                                      ? Container(
                                                                          height:
                                                                              smallBoxHeight,
                                                                          width:
                                                                              2.2 * smallBoxHeight,
                                                                          child:
                                                                              Text(
                                                                            '($player1TieBreak4)',
                                                                            textAlign:
                                                                                TextAlign.right,
                                                                            style:
                                                                                TextStyle(
                                                                              fontSize: smallFontSizeTie,
                                                                              color: AppColors.black,
                                                                              fontWeight: smallFontWeight,
                                                                            ),
                                                                          ),
                                                                        )
                                                                      : trigerTieAngles
                                                                          ? Container(
                                                                              height: smallBoxHeight,
                                                                              width: 2.2 * smallBoxHeight,
                                                                            )
                                                                          : Container(
                                                                              height: smallBoxHeight,
                                                                              width: smallBoxHeight,
                                                                            ),
                                                                ],
                                                              ),
                                                            ),
                                                            // 5 item -------------------------------------
                                                            AnimatedAlign(
                                                              duration: Duration(
                                                                  milliseconds:
                                                                      500),
                                                              alignment: Alignment(
                                                                  i1sAngle5Status
                                                                      ? getx(
                                                                          i1sAngle5)
                                                                      : 0,
                                                                  i1sAngle5Status
                                                                      ? gety(
                                                                          i1sAngle5)
                                                                      : 0),
                                                              child: Stack(
                                                                children: <
                                                                    Widget>[
                                                                  Container(
                                                                    height:
                                                                        smallBoxHeight,
                                                                    width:
                                                                        smallBoxHeight,
                                                                    alignment:
                                                                        Alignment
                                                                            .center,
                                                                    decoration:
                                                                        new BoxDecoration(
                                                                      color:
                                                                          smallBoxColor,
                                                                      border:
                                                                          new Border
                                                                              .all(
                                                                        width:
                                                                            smallBoxBorder,
                                                                        color:
                                                                            smallBorderColor,
                                                                      ),
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              smallBoxBorderRadius),
                                                                    ),
                                                                    child: Text(
                                                                      player1noOfWonsInSet5
                                                                          .toString(),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .center,
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            smallFontSize,
                                                                        color:
                                                                            smallFontColor,
                                                                        fontWeight:
                                                                            smallFontWeight,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  x1tie5
                                                                      ? Container(
                                                                          height:
                                                                              smallBoxHeight,
                                                                          width:
                                                                              2.2 * smallBoxHeight,
                                                                          child:
                                                                              Text(
                                                                            '($player1TieBreak5)',
                                                                            textAlign:
                                                                                TextAlign.right,
                                                                            style:
                                                                                TextStyle(
                                                                              fontSize: smallFontSizeTie,
                                                                              color: AppColors.black,
                                                                              fontWeight: smallFontWeight,
                                                                            ),
                                                                          ),
                                                                        )
                                                                      : trigerTieAngles
                                                                          ? Container(
                                                                              height: smallBoxHeight,
                                                                              width: 2.2 * smallBoxHeight,
                                                                            )
                                                                          : Container(
                                                                              height: smallBoxHeight,
                                                                              width: smallBoxHeight,
                                                                            ),
                                                                ],
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    )
                                                  : Container(),
                                              // Sets , Games , Points -------------------------------------
                                              // 1 seg -------------------------------------
                                              Align(
                                                alignment: Alignment(
                                                    getx(i1bAngle1),
                                                    gety(i1bAngle1)),
                                                child: Container(
                                                  height: bigMainBoxHeight,
                                                  width: bigMainBoxWidth,
                                                  child: Column(
                                                    children: <Widget>[
                                                      Container(
                                                        child: Text(
                                                          'SETS',
                                                          textAlign:
                                                              TextAlign.center,
                                                          style: TextStyle(
                                                            fontSize:
                                                                bigTitleFontSize,
                                                            color:
                                                                bigTitleFontColor,
                                                            fontWeight:
                                                                bigTitleFontWeight,
                                                          ),
                                                        ),
                                                      ),
                                                      Container(
                                                        height: bigBoxHeight,
                                                        width: bigBoxHeight,
                                                        alignment:
                                                            Alignment.center,
                                                        decoration:
                                                            new BoxDecoration(
                                                          color: bigBoxColor,
                                                          border:
                                                              new Border.all(
                                                            width: bigBoxBorder,
                                                            color:
                                                                bigBorderColor,
                                                          ),
                                                          borderRadius:
                                                              BorderRadius.circular(
                                                                  bigBoxBorderRadius),
                                                        ),
                                                        child: Text(
                                                          player1noOfSets
                                                              .toString(),
                                                          textAlign:
                                                              TextAlign.center,
                                                          style: TextStyle(
                                                            fontSize:
                                                                bigFontSize,
                                                            color: bigFontColor,
                                                            fontWeight:
                                                                bigFontWeight,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              // 2 seg -------------------------------------
                                              Align(
                                                alignment: Alignment(
                                                    getx(i1bAngle2),
                                                    gety(i1bAngle2)),
                                                child: Container(
                                                  height: bigMainBoxHeight,
                                                  width: bigMainBoxWidth,
                                                  child: Column(
                                                    children: <Widget>[
                                                      Container(
                                                        child: Text(
                                                          'GAMES',
                                                          textAlign:
                                                              TextAlign.center,
                                                          style: TextStyle(
                                                            fontSize:
                                                                bigTitleFontSize,
                                                            color:
                                                                bigTitleFontColor,
                                                            fontWeight:
                                                                bigTitleFontWeight,
                                                          ),
                                                        ),
                                                      ),
                                                      Container(
                                                        height: bigBoxHeight,
                                                        width: bigBoxHeight,
                                                        alignment:
                                                            Alignment.center,
                                                        decoration:
                                                            new BoxDecoration(
                                                          color: bigBoxColor,
                                                          border:
                                                              new Border.all(
                                                            width: bigBoxBorder,
                                                            color:
                                                                bigBorderColor,
                                                          ),
                                                          borderRadius:
                                                              BorderRadius.circular(
                                                                  bigBoxBorderRadius),
                                                        ),
                                                        child: Text(
                                                          player1noOfGames
                                                              .toString(),
                                                          textAlign:
                                                              TextAlign.center,
                                                          style: TextStyle(
                                                            fontSize:
                                                                bigFontSize,
                                                            color: bigFontColor,
                                                            fontWeight:
                                                                bigFontWeight,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              // 3 seg -------------------------------------
                                              Align(
                                                alignment: Alignment(
                                                    getx(i1bAngle3),
                                                    gety(i1bAngle3)),
                                                child: Container(
                                                  height: bigMainBoxHeight,
                                                  width: bigMainBoxWidth,
                                                  child: Column(
                                                    children: <Widget>[
                                                      Container(
                                                        child: Text(
                                                          'POINTS',
                                                          textAlign:
                                                              TextAlign.center,
                                                          style: TextStyle(
                                                            fontSize:
                                                                bigTitleFontSize,
                                                            color:
                                                                bigTitleFontColor,
                                                            fontWeight:
                                                                bigTitleFontWeight,
                                                          ),
                                                        ),
                                                      ),
                                                      Container(
                                                        height: bigBoxHeight,
                                                        width: bigBoxHeight,
                                                        alignment:
                                                            Alignment.center,
                                                        decoration:
                                                            new BoxDecoration(
                                                          color: bigBoxColor,
                                                          border:
                                                              new Border.all(
                                                            width: bigBoxBorder,
                                                            color:
                                                                bigBorderColor,
                                                          ),
                                                          borderRadius:
                                                              BorderRadius.circular(
                                                                  bigBoxBorderRadius),
                                                        ),
                                                        child: Text(
                                                          player1inAd
                                                              ? "Ad"
                                                              : player1noOfPoints
                                                                  .toString(),
                                                          textAlign:
                                                              TextAlign.center,
                                                          style: TextStyle(
                                                            fontSize:
                                                                bigFontSize,
                                                            color: bigFontColor,
                                                            fontWeight:
                                                                bigFontWeight,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        )),
                                    // Profile image -------------------------------------
                                    Align(
                                      alignment: Alignment.center,
                                      child: Container(
                                        width: profileImgWidth,
                                        height: profileImgHeight,
                                        child: Container(
                                          width: 130.0,
                                          height: 130.0,
                                          decoration: new BoxDecoration(
                                            color: AppColors.white,
                                            shape: BoxShape.circle,
                                            image: new DecorationImage(
                                              fit: BoxFit.cover,
                                              image: playerImage1 != null
                                                  ? getImageProvider(
                                                      File(playerImage1))
                                                  : new AssetImage(
                                                      "images/user.png"),
                                            ),
                                            border: Border.all(
                                              color: Colors.black,
                                              width: 3.0,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),

                                    Align(
                                        alignment: Alignment.bottomCenter,
                                        child: player1serve
                                            ? Padding(
                                                padding: actionButtonPadding2,
                                                child: Container(
                                                  height: aceButtonHeight,
                                                  width: actionBtnWidth,
                                                  alignment: Alignment.center,
                                                  child: Row(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    mainAxisSize:
                                                        MainAxisSize.max,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: <Widget>[
                                                      Padding(
                                                        padding: acePadding,
                                                        child: Container(
                                                          width: aceButtonWidth,
                                                          height:
                                                              aceButtonHeight,
                                                          child: RaisedButton(
                                                            padding:
                                                                EdgeInsets.all(
                                                                    0),
                                                            shape: RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            miniActionBtnBorderRadius),
                                                                side: BorderSide(
                                                                    color:
                                                                        miniActionBtnBorderColor,
                                                                    width:
                                                                        miniActionBtnBorderWidth,
                                                                    style: BorderStyle
                                                                        .solid)),
                                                            onPressed: () {
                                                              incrementACEPlayer1();
                                                              checkEnabled();
                                                            },
                                                            color:
                                                                aceDisabledBtn,
                                                            child: Text(
                                                              'ACE',
                                                              style: TextStyle(
                                                                fontSize:
                                                                    uerrorBtnFontSize,
                                                                fontWeight:
                                                                    miniActionBtnFontWeight,
                                                                color:
                                                                    miniActionBtnFontColor,
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ),

                                                      //player one fault d fault display
                                                      Padding(
                                                        padding: acePadding,
                                                        child: Container(
                                                          width: aceButtonWidth,
                                                          height:
                                                              aceButtonHeight,
                                                          child: RaisedButton(
                                                            padding:
                                                                EdgeInsets.all(
                                                                    0),
                                                            shape: new RoundedRectangleBorder(
                                                                borderRadius:
                                                                    new BorderRadius
                                                                            .circular(
                                                                        miniActionBtnBorderRadius),
                                                                side: BorderSide(
                                                                    color:
                                                                        miniActionBtnBorderColor,
                                                                    width:
                                                                        miniActionBtnBorderWidth,
                                                                    style: BorderStyle
                                                                        .solid)),
                                                            onPressed: () {
                                                              faultPlayer1();
                                                              checkEnabled();
                                                            },
                                                            color:
                                                                aceDisabledBtn,
                                                            child: Text(
                                                              player1FaultError,
                                                              style: TextStyle(
                                                                fontSize:
                                                                    uerrorBtnFontSize,
                                                                fontWeight:
                                                                    miniActionBtnFontWeight,
                                                                color:
                                                                    aceDisabledBtnFont,
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              )
                                            : Padding(
                                                padding: actionButtonPadding2,
                                                child: Container(
                                                  height: aceButtonHeight,
                                                ),
                                              )),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                  ),
                ),

                Align(
                  child: Container(
                    height: centerBtnheight,
                    width: centerBtnWidth,
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          width: centerMiniBtnWidth,
                          height: centerMiniBtnHeight,
                          padding: EdgeInsets.only(left: 6, right: 6),
                          decoration: new BoxDecoration(
                            borderRadius: BorderRadius.circular(
                                centerMiniBtnBorderRadius),
                            color: centerMiniBtnColor,
                            border: new Border.all(
                                color: centerMiniBtnBorderColor,
                                width: centerMiniBtnBorderWidth),
                          ),
                          child: Stack(
                            children: <Widget>[
                              Align(
                                alignment: Alignment.center,
                                child: Text(
                                  // '05 : 26 : 03',
                                  timeInNiceFormat,
                                  style: TextStyle(
                                    fontSize: centerMiniBtnFontSize,
                                    fontWeight: centerMiniBtnFontWeight,
                                    color: centerMiniBtnFontColor,
                                  ),
                                ),
                              ),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Container(
                                  height: 30,
                                  width: 30,
                                  child: Stack(
                                    children: <Widget>[
                                      Positioned(
                                        width: 30,
                                        height: 30,
                                        child: gameFinish ? Container() : play,
                                      ),
                                      Container(
                                        width: 30,
                                        height: 30,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(30),
                                        ),
                                        child: gameFinish
                                            ? Container()
                                            : RawMaterialButton(
                                                splashColor:
                                                    AppColors.ternary_color,
                                                onPressed: () {
                                                  if (timerStatus) {
                                                    togglePlayState();

                                                    showDialog(
                                                        context: context,
                                                        builder: (context) =>
                                                            AppPopup(
                                                                context,
                                                                ScoreValues
                                                                    .pause,
                                                                [
                                                                  ScoreValues
                                                                      .pause
                                                                ],
                                                                timerPauseCallback));
                                                  } else {
                                                    widget.togglePlayStateMain(
                                                        false,
                                                        timeInSeconds,
                                                        false,
                                                        matchId);
                                                    setState(() {
                                                      Const.timeChange = false;
                                                    });
                                                    togglePlayState();
                                                  }
                                                },
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(30.0)),
                                              ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  height: 26,
                                  width: 26,
                                  child: RotationTransition(
                                    turns: animation,
                                    child: hrglass,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: centerMiniBtnWidth,
                          height: centerMiniBtnHeight,
                          padding: EdgeInsets.only(left: 20, right: 20),
                          decoration: new BoxDecoration(
                            borderRadius: BorderRadius.circular(
                                centerMiniBtnBorderRadius),
                            color: centerMiniBtnColor,
                            border: new Border.all(
                                color: centerMiniBtnBorderColor,
                                width: centerMiniBtnBorderWidth),
                          ),
                          child: Stack(
                            children: <Widget>[
                              Align(
                                alignment: Alignment.center,
                                child: Text(
                                  noOfTries.toString(),
                                  style: TextStyle(
                                    fontSize: centerMiniBtnFontSize,
                                    fontWeight: centerMiniBtnFontWeight,
                                    color: centerMiniBtnFontColor,
                                  ),
                                ),
                              ),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Container(
                                  height: 30,
                                  width: 30,
                                  child: Stack(
                                    children: <Widget>[
                                      Positioned(
                                        width: 30,
                                        height: 30,
                                        child: plus,
                                      ),
                                      Container(
                                        width: 30,
                                        height: 30,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(30),
                                        ),
                                        child: RawMaterialButton(
                                          splashColor: AppColors.ternary_color,
                                          onPressed: () {
                                            noOfTriesAdder();
                                            checkEnabled();
                                          },
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      30.0)),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  height: 30,
                                  width: 30,
                                  child: Stack(
                                    children: <Widget>[
                                      Positioned(
                                        width: 30,
                                        height: 30,
                                        child: minus,
                                      ),
                                      Container(
                                        width: 30,
                                        height: 30,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(30),
                                        ),
                                        child: RawMaterialButton(
                                          splashColor: AppColors.ternary_color,
                                          onPressed: () {
                                            noOfTriesReducer();
                                            checkEnabled();
                                          },
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      30.0)),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),

                // Player 2 Starting -----------------------------------------
                AnimatedContainer(
                  height: player2serve ? mainCardHeight : mainCardHeightSp,
                  duration: Duration(milliseconds: 50),
                  child: Container(
                    margin: EdgeInsets.all(mainCardMargin),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(mainCardBorderRadius),
                      color: mainCardBackColor,
                      border: Border.all(
                        width: mainCardBorderWidth,
                        color: mainCardBorderColor,
                      ),
                    ),
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20.0, right: 20),
                          child: Container(
                            margin: EdgeInsets.only(top: 0),
                            child: Text(
                              player2No + " - " + player2Name,
                              textAlign: TextAlign.center,
                              overflow: TextOverflow.fade,
                              maxLines: 1,
                              softWrap: false,
                              style: TextStyle(
                                  height: 0.9,
                                  fontSize: nameFontSize,
                                  color: nameFontColor,
                                  fontWeight: nameFontWeight),
                            ),
                          ),
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          // mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            //left side block
                            Expanded(
                              child: AnimatedContainer(
                                  duration: Duration(milliseconds: 50),
                                  height: player2serve
                                      ? leftContainerHeight
                                      : leftContainerHeightSp,
                                  width: double.infinity,
                                  child: Column(
                                    children: <Widget>[
                                      Expanded(
                                        child: Container(
                                          width: racketImgCoverWidth,
                                          height: racketImgCoverHeight,
                                          child: Stack(
                                            children: <Widget>[
                                              GestureDetector(
                                                onTap: () {
                                                  setState(() {
                                                    player2serve = true;
                                                    player1serve = false;
                                                    player1FaultError =
                                                        ScoreValues.flt;
                                                    player2FaultError =
                                                        ScoreValues.flt;
                                                  });
                                                  widget.funcSetplayerServe(2);
                                                  checkEnabled();
                                                },
                                                child: Container(
                                                    width: racketImgWidth,
                                                    height: racketImgHeight,
                                                    decoration: BoxDecoration(),
                                                    child: racket),
                                              ),
                                              player2serve
                                                  ? Positioned(
                                                      right: 0,
                                                      bottom: 0,
                                                      child: Text(
                                                        'Serve',
                                                        style: TextStyle(
                                                          fontSize:
                                                              wordServeFontSize,
                                                          fontWeight:
                                                              wordServeFontWeight,
                                                          color:
                                                              wordServeFontColor,
                                                        ),
                                                      ),
                                                    )
                                                  : Container(),
                                            ],
                                          ),
                                        ),
                                      ),

                                      //winner player two
                                      Flexible(
                                        child: Align(
                                          child: Padding(
                                            padding: actionButtonPadding2,
                                            child: Container(
                                              height: actionBtnHeight,
                                              width: actionBtnWidth,
                                              alignment: Alignment.center,
                                              child: Container(
                                                width: double.infinity,
                                                child: RaisedButton(
                                                  padding: EdgeInsets.all(0),
                                                  shape: new RoundedRectangleBorder(
                                                      borderRadius: new BorderRadius
                                                              .circular(
                                                          miniActionBtnBorderRadius),
                                                      side: BorderSide(
                                                          color:
                                                              miniActionBtnBorderColor,
                                                          width:
                                                              miniActionBtnBorderWidth,
                                                          style: BorderStyle
                                                              .solid)),
                                                  onPressed: () {
                                                    winnerTypeSetup(
                                                        ScoreValues.winner);
                                                    incrementPlayer2();
                                                    checkEnabled();
                                                  },
                                                  color: miniActionBtnColor,
                                                  child: Stack(
                                                    children: <Widget>[
                                                      Container(
                                                        height: userErrorHeight,
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  right: 0.0),
                                                          child: Align(
                                                            alignment: Alignment
                                                                .centerRight,
                                                            child:
                                                                GestureDetector(
                                                              onTap: () {
                                                                StarMenuController
                                                                    .displayStarMenu(
                                                                        _buildMenu(
                                                                          secondWinnerBtnKey,
                                                                          MenuShape
                                                                              .grid,
                                                                          0.0,
                                                                          3,
                                                                        ),
                                                                        secondWinnerBtnKey);

                                                                checkEnabled();
                                                              },
                                                              child: Container(
                                                                //color: Colors.green,
                                                                width:
                                                                    actionBtnHeight,
                                                                height:
                                                                    actionBtnHeight,
                                                                child: Align(
                                                                  //Global key
                                                                  key:
                                                                      secondWinnerBtnKey,
                                                                  alignment:
                                                                      Alignment
                                                                          .center,
                                                                  child:
                                                                      Container(
                                                                    width: .6 *
                                                                        actionBtnHeight,
                                                                    height: .6 *
                                                                        actionBtnHeight,
                                                                    child: ball,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            winnerButtonFontPadding,
                                                        child: Align(
                                                          alignment:
                                                              Alignment.center,
                                                          child: Text(
                                                            'WINNER',
                                                            style: TextStyle(
                                                              fontSize:
                                                                  uerrorBtnFontSize,
                                                              fontWeight:
                                                                  miniActionBtnFontWeight,
                                                              color:
                                                                  miniActionBtnFontColor,
                                                            ),
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),

                                      //player two u.err button
                                      Flexible(
                                        child: Align(
                                          child: Padding(
                                            padding: actionButtonPadding,
                                            child: Container(
                                              height: actionBtnHeight,
                                              width: actionBtnWidth,
                                              alignment: Alignment.center,
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                mainAxisSize: MainAxisSize.max,
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  //u err player
                                                  Padding(
                                                    padding: uErrorPadding,
                                                    child: Container(
                                                      width: actionBtnWidth,
                                                      height: userErrorHeight,
                                                      child: RaisedButton(
                                                        padding:
                                                            EdgeInsets.all(0),
                                                        shape: new RoundedRectangleBorder(
                                                            borderRadius:
                                                                new BorderRadius
                                                                        .circular(
                                                                    miniActionBtnBorderRadius),
                                                            side: BorderSide(
                                                                color:
                                                                    miniActionBtnBorderColor,
                                                                width:
                                                                    miniActionBtnBorderWidth,
                                                                style:
                                                                    BorderStyle
                                                                        .solid)),
                                                        onPressed: () {
                                                          uerrorPlayer2();
                                                          checkEnabled();
                                                        },
                                                        color:
                                                            miniActionBtnColor,
                                                        child: Stack(
                                                          children: <Widget>[
                                                            Container(
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        right:
                                                                            0.0),
                                                                child: Align(
                                                                  alignment:
                                                                      Alignment
                                                                          .centerRight,
                                                                  child:
                                                                      GestureDetector(
                                                                    onTap: () {
                                                                      StarMenuController.displayStarMenu(
                                                                          _buildMenu(
                                                                            secondUerrorBtnKey,
                                                                            MenuShape.grid,
                                                                            0.0,
                                                                            4,
                                                                          ),
                                                                          secondUerrorBtnKey);
                                                                      checkEnabled();
                                                                    },
                                                                    child:
                                                                        Container(
                                                                      //color: Colors.red,
                                                                      key:
                                                                          secondUerrorBtnKey,
                                                                      width:
                                                                          actionBtnHeight,
                                                                      height:
                                                                          actionBtnHeight,
                                                                      child:
                                                                          Align(
                                                                        alignment:
                                                                            Alignment.center,
                                                                        child:
                                                                            Container(
                                                                          width:
                                                                              .6 * actionBtnHeight,
                                                                          height:
                                                                              .6 * actionBtnHeight,
                                                                          child:
                                                                              ball,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                            Padding(
                                                              padding:
                                                                  winnerButtonFontPadding,
                                                              child: Align(
                                                                alignment:
                                                                    Alignment
                                                                        .center,
                                                                child: Text(
                                                                  'U.ERR',
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        uerrorBtnFontSize,
                                                                    fontWeight:
                                                                        miniActionBtnFontWeight,
                                                                    color:
                                                                        miniActionBtnFontColor,
                                                                  ),
                                                                ),
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),

                                      //player two f.err
                                      Flexible(
                                        child: Align(
                                          child: Padding(
                                            padding: actionButtonPadding,
                                            child: Container(
                                              height: actionBtnHeight,
                                              width: actionBtnWidth,
                                              alignment: Alignment.center,
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                mainAxisSize: MainAxisSize.max,
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Padding(
                                                    padding: uErrorPadding,
                                                    child: Container(
                                                      width: actionBtnWidth,
                                                      height: userErrorHeight,
                                                      child: RaisedButton(
                                                        padding:
                                                            EdgeInsets.all(0),
                                                        shape: new RoundedRectangleBorder(
                                                            borderRadius:
                                                                new BorderRadius
                                                                        .circular(
                                                                    miniActionBtnBorderRadius),
                                                            side: BorderSide(
                                                                color:
                                                                    miniActionBtnBorderColor,
                                                                width:
                                                                    miniActionBtnBorderWidth,
                                                                style:
                                                                    BorderStyle
                                                                        .solid)),
                                                        onPressed: () {
                                                          ferrorPlayer2();
                                                          checkEnabled();
                                                        },
                                                        color:
                                                            miniActionBtnColor,
                                                        child: Stack(
                                                          children: <Widget>[
                                                            Container(
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        right:
                                                                            0.0),
                                                                child: Align(
                                                                  alignment:
                                                                      Alignment
                                                                          .centerRight,
                                                                  child:
                                                                      GestureDetector(
                                                                    onTap: () {
                                                                      // ferrorPlayer2();
                                                                      // checkEnabled();
                                                                      StarMenuController.displayStarMenu(
                                                                          _buildMenu(
                                                                            secondFerrorBtnKey,
                                                                            MenuShape.grid,
                                                                            0.0,
                                                                            6,
                                                                          ),
                                                                          secondFerrorBtnKey);
                                                                      checkEnabled();
                                                                    },
                                                                    child:
                                                                        Container(
                                                                      //color: Colors.red,
                                                                      key:
                                                                          secondFerrorBtnKey,
                                                                      width:
                                                                          actionBtnHeight,
                                                                      height:
                                                                          actionBtnHeight,
                                                                      child:
                                                                          Align(
                                                                        alignment:
                                                                            Alignment.center,
                                                                        child:
                                                                            Container(
                                                                          width:
                                                                              .6 * actionBtnHeight,
                                                                          height:
                                                                              .6 * actionBtnHeight,
                                                                          child:
                                                                              ball,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                            Padding(
                                                              padding:
                                                                  winnerButtonFontPadding,
                                                              child: Align(
                                                                alignment:
                                                                    Alignment
                                                                        .center,
                                                                child: Text(
                                                                  'F.ERR',
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        uerrorBtnFontSize,
                                                                    fontWeight:
                                                                        miniActionBtnFontWeight,
                                                                    color:
                                                                        miniActionBtnFontColor,
                                                                  ),
                                                                ),
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  )),
                            ),
                            // Right Side Block -------------------------------------
                            Expanded(
                              child: Container(
                                height: rightContainerHeight,
                                child: Stack(
                                  children: <Widget>[
                                    // Bigger Circle -------------------------------------
                                    Align(
                                        alignment: Alignment.center,
                                        child: Container(
                                          width: outerDiameter,
                                          height: outerDiameter,
                                          child: Stack(
                                            children: <Widget>[
                                              // no of games -------------------------------------
                                              subItem2Show
                                                  ? Align(
                                                      alignment:
                                                          Alignment(0, 0),
                                                      child: Container(
                                                        height: innerDiameter,
                                                        width: innerDiameter,
                                                        child: Stack(
                                                          children: <Widget>[
                                                            // 1 item -------------------------------------
                                                            AnimatedAlign(
                                                              duration: Duration(
                                                                  milliseconds:
                                                                      500),
                                                              alignment: Alignment(
                                                                  i2sAngle1Status
                                                                      ? getx(
                                                                          i2sAngle1)
                                                                      : 0,
                                                                  i2sAngle1Status
                                                                      ? gety(
                                                                          i2sAngle1)
                                                                      : 0),
                                                              child: Stack(
                                                                children: <
                                                                    Widget>[
                                                                  Container(
                                                                    height:
                                                                        smallBoxHeight,
                                                                    width:
                                                                        smallBoxHeight,
                                                                    alignment:
                                                                        Alignment
                                                                            .center,
                                                                    decoration:
                                                                        new BoxDecoration(
                                                                      color:
                                                                          smallBoxColor,
                                                                      border:
                                                                          new Border
                                                                              .all(
                                                                        width:
                                                                            smallBoxBorder,
                                                                        color:
                                                                            smallBorderColor,
                                                                      ),
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              smallBoxBorderRadius),
                                                                    ),
                                                                    child:
                                                                        Container(
                                                                      child:
                                                                          Text(
                                                                        player2noOfWonsInSet1
                                                                            .toString(),
                                                                        textAlign:
                                                                            TextAlign.center,
                                                                        style:
                                                                            TextStyle(
                                                                          fontSize:
                                                                              smallFontSize,
                                                                          color:
                                                                              smallFontColor,
                                                                          fontWeight:
                                                                              smallFontWeight,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  x2tie1
                                                                      ? Container(
                                                                          height:
                                                                              smallBoxHeight,
                                                                          width:
                                                                              2.2 * smallBoxHeight,
                                                                          child:
                                                                              Text(
                                                                            '($player2TieBreak1)',
                                                                            textAlign:
                                                                                TextAlign.right,
                                                                            style:
                                                                                TextStyle(
                                                                              fontSize: smallFontSizeTie,
                                                                              color: AppColors.black,
                                                                              fontWeight: smallFontWeight,
                                                                            ),
                                                                          ),
                                                                        )
                                                                      : trigerTieAngles
                                                                          ? Container(
                                                                              height: smallBoxHeight,
                                                                              width: 2.2 * smallBoxHeight,
                                                                            )
                                                                          : Container(
                                                                              height: smallBoxHeight,
                                                                              width: smallBoxHeight,
                                                                            ),
                                                                ],
                                                              ),
                                                            ),
                                                            // 2 item -------------------------------------
                                                            AnimatedAlign(
                                                              duration: Duration(
                                                                  milliseconds:
                                                                      500),
                                                              alignment: Alignment(
                                                                  i2sAngle2Status
                                                                      ? getx(
                                                                          i2sAngle2)
                                                                      : 0,
                                                                  i2sAngle2Status
                                                                      ? gety(
                                                                          i2sAngle2)
                                                                      : 0),
                                                              child: Stack(
                                                                children: <
                                                                    Widget>[
                                                                  Container(
                                                                    height:
                                                                        smallBoxHeight,
                                                                    width:
                                                                        smallBoxHeight,
                                                                    alignment:
                                                                        Alignment
                                                                            .center,
                                                                    decoration:
                                                                        new BoxDecoration(
                                                                      color:
                                                                          smallBoxColor,
                                                                      border:
                                                                          new Border
                                                                              .all(
                                                                        width:
                                                                            smallBoxBorder,
                                                                        color:
                                                                            smallBorderColor,
                                                                      ),
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              smallBoxBorderRadius),
                                                                    ),
                                                                    child: Text(
                                                                      player2noOfWonsInSet2
                                                                          .toString(),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .center,
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            smallFontSize,
                                                                        color:
                                                                            smallFontColor,
                                                                        fontWeight:
                                                                            smallFontWeight,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  x2tie2
                                                                      ? Container(
                                                                          height:
                                                                              smallBoxHeight,
                                                                          width:
                                                                              2.2 * smallBoxHeight,
                                                                          child:
                                                                              Text(
                                                                            '($player2TieBreak2)',
                                                                            textAlign:
                                                                                TextAlign.right,
                                                                            style:
                                                                                TextStyle(
                                                                              fontSize: smallFontSizeTie,
                                                                              color: AppColors.black,
                                                                              fontWeight: smallFontWeight,
                                                                            ),
                                                                          ),
                                                                        )
                                                                      : trigerTieAngles
                                                                          ? Container(
                                                                              height: smallBoxHeight,
                                                                              width: 2.2 * smallBoxHeight,
                                                                            )
                                                                          : Container(
                                                                              height: smallBoxHeight,
                                                                              width: smallBoxHeight,
                                                                            ),
                                                                ],
                                                              ),
                                                            ),
                                                            // 3 item -------------------------------------
                                                            AnimatedAlign(
                                                              duration: Duration(
                                                                  milliseconds:
                                                                      500),
                                                              alignment: Alignment(
                                                                  i2sAngle3Status
                                                                      ? getx(
                                                                          i2sAngle3)
                                                                      : 0,
                                                                  i2sAngle3Status
                                                                      ? gety(
                                                                          i2sAngle3)
                                                                      : 0),
                                                              child: Stack(
                                                                children: <
                                                                    Widget>[
                                                                  Container(
                                                                    height:
                                                                        smallBoxHeight,
                                                                    width:
                                                                        smallBoxHeight,
                                                                    alignment:
                                                                        Alignment
                                                                            .center,
                                                                    decoration:
                                                                        new BoxDecoration(
                                                                      color:
                                                                          smallBoxColor,
                                                                      border:
                                                                          new Border
                                                                              .all(
                                                                        width:
                                                                            smallBoxBorder,
                                                                        color:
                                                                            smallBorderColor,
                                                                      ),
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              smallBoxBorderRadius),
                                                                    ),
                                                                    child: Text(
                                                                      player2noOfWonsInSet3
                                                                          .toString(),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .center,
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            smallFontSize,
                                                                        color:
                                                                            smallFontColor,
                                                                        fontWeight:
                                                                            smallFontWeight,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  x2tie3
                                                                      ? Container(
                                                                          height:
                                                                              smallBoxHeight,
                                                                          width:
                                                                              2.2 * smallBoxHeight,
                                                                          child:
                                                                              Text(
                                                                            '($player2TieBreak3)',
                                                                            textAlign:
                                                                                TextAlign.right,
                                                                            style:
                                                                                TextStyle(
                                                                              fontSize: smallFontSizeTie,
                                                                              color: AppColors.black,
                                                                              fontWeight: smallFontWeight,
                                                                            ),
                                                                          ),
                                                                        )
                                                                      : trigerTieAngles
                                                                          ? Container(
                                                                              height: smallBoxHeight,
                                                                              width: 2.2 * smallBoxHeight,
                                                                            )
                                                                          : Container(
                                                                              height: smallBoxHeight,
                                                                              width: smallBoxHeight,
                                                                            ),
                                                                ],
                                                              ),
                                                            ),
                                                            // 4 item -------------------------------------
                                                            AnimatedAlign(
                                                              duration: Duration(
                                                                  milliseconds:
                                                                      500),
                                                              alignment: Alignment(
                                                                  i2sAngle4Status
                                                                      ? getx(
                                                                          i2sAngle4)
                                                                      : 0,
                                                                  i2sAngle4Status
                                                                      ? gety(
                                                                          i2sAngle4)
                                                                      : 0),
                                                              child: Stack(
                                                                children: <
                                                                    Widget>[
                                                                  Container(
                                                                    height:
                                                                        smallBoxHeight,
                                                                    width:
                                                                        smallBoxHeight,
                                                                    alignment:
                                                                        Alignment
                                                                            .center,
                                                                    decoration:
                                                                        new BoxDecoration(
                                                                      color:
                                                                          smallBoxColor,
                                                                      border:
                                                                          new Border
                                                                              .all(
                                                                        width:
                                                                            smallBoxBorder,
                                                                        color:
                                                                            smallBorderColor,
                                                                      ),
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              smallBoxBorderRadius),
                                                                    ),
                                                                    child: Text(
                                                                      player2noOfWonsInSet4
                                                                          .toString(),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .center,
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            smallFontSize,
                                                                        color:
                                                                            smallFontColor,
                                                                        fontWeight:
                                                                            smallFontWeight,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  x2tie4
                                                                      ? Container(
                                                                          height:
                                                                              smallBoxHeight,
                                                                          width:
                                                                              2.2 * smallBoxHeight,
                                                                          child:
                                                                              Text(
                                                                            '($player2TieBreak4)',
                                                                            textAlign:
                                                                                TextAlign.right,
                                                                            style:
                                                                                TextStyle(
                                                                              fontSize: smallFontSizeTie,
                                                                              color: AppColors.black,
                                                                              fontWeight: smallFontWeight,
                                                                            ),
                                                                          ),
                                                                        )
                                                                      : trigerTieAngles
                                                                          ? Container(
                                                                              height: smallBoxHeight,
                                                                              width: 2.2 * smallBoxHeight,
                                                                            )
                                                                          : Container(
                                                                              height: smallBoxHeight,
                                                                              width: smallBoxHeight,
                                                                            ),
                                                                ],
                                                              ),
                                                            ),
                                                            // 5 item -------------------------------------
                                                            AnimatedAlign(
                                                              duration: Duration(
                                                                  milliseconds:
                                                                      500),
                                                              alignment: Alignment(
                                                                  i2sAngle5Status
                                                                      ? getx(
                                                                          i2sAngle5)
                                                                      : 0,
                                                                  i2sAngle5Status
                                                                      ? gety(
                                                                          i2sAngle5)
                                                                      : 0),
                                                              child: Stack(
                                                                children: <
                                                                    Widget>[
                                                                  Container(
                                                                    height:
                                                                        smallBoxHeight,
                                                                    width:
                                                                        smallBoxHeight,
                                                                    alignment:
                                                                        Alignment
                                                                            .center,
                                                                    decoration:
                                                                        new BoxDecoration(
                                                                      color:
                                                                          smallBoxColor,
                                                                      border:
                                                                          new Border
                                                                              .all(
                                                                        width:
                                                                            smallBoxBorder,
                                                                        color:
                                                                            smallBorderColor,
                                                                      ),
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              smallBoxBorderRadius),
                                                                    ),
                                                                    child: Text(
                                                                      player2noOfWonsInSet5
                                                                          .toString(),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .center,
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            smallFontSize,
                                                                        color:
                                                                            smallFontColor,
                                                                        fontWeight:
                                                                            smallFontWeight,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  x2tie5
                                                                      ? Container(
                                                                          height:
                                                                              smallBoxHeight,
                                                                          width:
                                                                              2.2 * smallBoxHeight,
                                                                          child:
                                                                              Text(
                                                                            '($player2TieBreak5)',
                                                                            textAlign:
                                                                                TextAlign.right,
                                                                            style:
                                                                                TextStyle(
                                                                              fontSize: smallFontSizeTie,
                                                                              color: AppColors.black,
                                                                              fontWeight: smallFontWeight,
                                                                            ),
                                                                          ),
                                                                        )
                                                                      : trigerTieAngles
                                                                          ? Container(
                                                                              height: smallBoxHeight,
                                                                              width: 2.2 * smallBoxHeight,
                                                                            )
                                                                          : Container(
                                                                              height: smallBoxHeight,
                                                                              width: smallBoxHeight,
                                                                            ),
                                                                ],
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    )
                                                  : Container(),
                                              // Sets , Games , Points -------------------------------------
                                              // 1 seg -------------------------------------
                                              Align(
                                                alignment: Alignment(
                                                    getx(i2bAngle1),
                                                    gety(i2bAngle1)),
                                                child: Container(
                                                  height: bigMainBoxHeight,
                                                  width: bigMainBoxWidth,
                                                  child: Column(
                                                    children: <Widget>[
                                                      Container(
                                                        child: Text(
                                                          'SETS',
                                                          textAlign:
                                                              TextAlign.center,
                                                          style: TextStyle(
                                                            fontSize:
                                                                bigTitleFontSize,
                                                            color:
                                                                bigTitleFontColor,
                                                            fontWeight:
                                                                bigTitleFontWeight,
                                                          ),
                                                        ),
                                                      ),
                                                      Container(
                                                        height: bigBoxHeight,
                                                        width: bigBoxHeight,
                                                        alignment:
                                                            Alignment.center,
                                                        decoration:
                                                            new BoxDecoration(
                                                          color: bigBoxColor,
                                                          border:
                                                              new Border.all(
                                                            width: bigBoxBorder,
                                                            color:
                                                                bigBorderColor,
                                                          ),
                                                          borderRadius:
                                                              BorderRadius.circular(
                                                                  bigBoxBorderRadius),
                                                        ),
                                                        child: Text(
                                                          player2noOfSets
                                                              .toString(),
                                                          textAlign:
                                                              TextAlign.center,
                                                          style: TextStyle(
                                                            fontSize:
                                                                bigFontSize,
                                                            color: bigFontColor,
                                                            fontWeight:
                                                                bigFontWeight,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              // 2 seg -------------------------------------
                                              Align(
                                                alignment: Alignment(
                                                    getx(i2bAngle2),
                                                    gety(i2bAngle2)),
                                                child: Container(
                                                  height: bigMainBoxHeight,
                                                  width: bigMainBoxWidth,
                                                  child: Column(
                                                    children: <Widget>[
                                                      Container(
                                                        child: Text(
                                                          'GAMES',
                                                          textAlign:
                                                              TextAlign.center,
                                                          style: TextStyle(
                                                            fontSize:
                                                                bigTitleFontSize,
                                                            color:
                                                                bigTitleFontColor,
                                                            fontWeight:
                                                                bigTitleFontWeight,
                                                          ),
                                                        ),
                                                      ),
                                                      Container(
                                                        height: bigBoxHeight,
                                                        width: bigBoxHeight,
                                                        alignment:
                                                            Alignment.center,
                                                        decoration:
                                                            new BoxDecoration(
                                                          color: bigBoxColor,
                                                          border:
                                                              new Border.all(
                                                            width: bigBoxBorder,
                                                            color:
                                                                bigBorderColor,
                                                          ),
                                                          borderRadius:
                                                              BorderRadius.circular(
                                                                  bigBoxBorderRadius),
                                                        ),
                                                        child: Text(
                                                          player2noOfGames
                                                              .toString(),
                                                          textAlign:
                                                              TextAlign.center,
                                                          style: TextStyle(
                                                            fontSize:
                                                                bigFontSize,
                                                            color: bigFontColor,
                                                            fontWeight:
                                                                bigFontWeight,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              // 3 seg -------------------------------------
                                              Align(
                                                alignment: Alignment(
                                                    getx(i2bAngle3),
                                                    gety(i2bAngle3)),
                                                child: Container(
                                                  height: bigMainBoxHeight,
                                                  width: bigMainBoxWidth,
                                                  child: Column(
                                                    children: <Widget>[
                                                      Container(
                                                        child: Text(
                                                          'POINTS',
                                                          textAlign:
                                                              TextAlign.center,
                                                          style: TextStyle(
                                                            fontSize:
                                                                bigTitleFontSize,
                                                            color:
                                                                bigTitleFontColor,
                                                            fontWeight:
                                                                bigTitleFontWeight,
                                                          ),
                                                        ),
                                                      ),
                                                      Container(
                                                        height: bigBoxHeight,
                                                        width: bigBoxHeight,
                                                        alignment:
                                                            Alignment.center,
                                                        decoration:
                                                            new BoxDecoration(
                                                          color: bigBoxColor,
                                                          border:
                                                              new Border.all(
                                                            width: bigBoxBorder,
                                                            color:
                                                                bigBorderColor,
                                                          ),
                                                          borderRadius:
                                                              BorderRadius.circular(
                                                                  bigBoxBorderRadius),
                                                        ),
                                                        child: Text(
                                                          player2inAd
                                                              ? "Ad"
                                                              : player2noOfPoints
                                                                  .toString(),
                                                          textAlign:
                                                              TextAlign.center,
                                                          style: TextStyle(
                                                            fontSize:
                                                                bigFontSize,
                                                            color: bigFontColor,
                                                            fontWeight:
                                                                bigFontWeight,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        )),
                                    // Profile image -------------------------------------
                                    Align(
                                      alignment: Alignment.center,
                                      child: Container(
                                        width: profileImgWidth,
                                        height: profileImgHeight,
                                        child: Container(
                                          width: 130.0,
                                          height: 130.0,
                                          decoration: new BoxDecoration(
                                            color: AppColors.white,
                                            shape: BoxShape.circle,
                                            image: new DecorationImage(
                                                fit: BoxFit.cover,
                                                image: playerImage2 != null
                                                    ? getImageProvider(
                                                        File(playerImage2))
                                                    : new AssetImage(
                                                        "images/user.png")),
                                            border: Border.all(
                                              color: Colors.black,
                                              width: 3.0,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),

                                    //player two ace button
                                    Align(
                                        alignment: Alignment.bottomCenter,
                                        child: player2serve
                                            ? Padding(
                                                padding: actionButtonPadding2,
                                                child: Container(
                                                  height: aceButtonHeight,
                                                  width: actionBtnWidth,
                                                  alignment: Alignment.center,
                                                  child: Row(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    mainAxisSize:
                                                        MainAxisSize.max,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: <Widget>[
                                                      Padding(
                                                        padding: acePadding,
                                                        child: Container(
                                                          width: aceButtonWidth,
                                                          height:
                                                              aceButtonHeight,
                                                          child: RaisedButton(
                                                            padding:
                                                                EdgeInsets.all(
                                                                    0),
                                                            shape: RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            miniActionBtnBorderRadius),
                                                                side: BorderSide(
                                                                    color:
                                                                        miniActionBtnBorderColor,
                                                                    width:
                                                                        miniActionBtnBorderWidth,
                                                                    style: BorderStyle
                                                                        .solid)),
                                                            onPressed: () {
                                                              incrementACEPlayer2();
                                                              checkEnabled();
                                                            },
                                                            color:
                                                                aceDisabledBtn,
                                                            child: Text(
                                                              'ACE',
                                                              style: TextStyle(
                                                                fontSize:
                                                                    uerrorBtnFontSize,
                                                                fontWeight:
                                                                    miniActionBtnFontWeight,
                                                                color:
                                                                    miniActionBtnFontColor,
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ),

                                                      //player two fault fault display
                                                      Padding(
                                                        padding: acePadding,
                                                        child: Container(
                                                          width: aceButtonWidth,
                                                          height:
                                                              aceButtonHeight,
                                                          child: RaisedButton(
                                                            padding:
                                                                EdgeInsets.all(
                                                                    0),
                                                            shape: new RoundedRectangleBorder(
                                                                borderRadius:
                                                                    new BorderRadius
                                                                            .circular(
                                                                        miniActionBtnBorderRadius),
                                                                side: BorderSide(
                                                                    color:
                                                                        miniActionBtnBorderColor,
                                                                    width:
                                                                        miniActionBtnBorderWidth,
                                                                    style: BorderStyle
                                                                        .solid)),
                                                            onPressed: () {
                                                              faultPlayer2();
                                                              checkEnabled();
                                                            },
                                                            color:
                                                                aceDisabledBtn,
                                                            child: Text(
                                                              player2FaultError,
                                                              style: TextStyle(
                                                                fontSize:
                                                                    uerrorBtnFontSize,
                                                                fontWeight:
                                                                    miniActionBtnFontWeight,
                                                                color:
                                                                    aceDisabledBtnFont,
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              )
                                            : Padding(
                                                padding: actionButtonPadding2,
                                                child: Container(
                                                  height: aceButtonHeight,
                                                ),
                                              )),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                  ),
                ),

                Align(
                  //stats

                  child: Container(
                    height: otherScreenBtnHeight,
                    width: otherScreenBtnWidth,
                    margin: EdgeInsets.only(bottom: 50),
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        GestureDetector(
                          child: Container(
                            width: otherScreenMiniBtnWidth,
                            height: otherScreenMiniBtnHeight,
                            decoration: new BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                  otherScreenBorderRadius),
                              color: otherScreenBtnColor,
                              border: new Border.all(
                                  color: otherScreenBorderColor,
                                  width: otherScreenBorderWidth),
                            ),
                            child: Stack(
                              children: <Widget>[
                                Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    height: otherScreenImgHeight,
                                    width: otherScreenImgWidth,
                                    child: report,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          onTap: () {
                            widget.onArgumentSaving({
                              "matchID": matchId,
                              "player1": player1Id,
                              "player2": player2Id,
                              "date": dateObject.toString(),
                              "time": timeInSeconds,
                            });
                            if (Const.buttonCount < 9) {
                              Const.buttonCount = Const.buttonCount + 1;
                            } else {
                              Const.buttonCount = 0;
                            }
                            widget.onSelected(RoutingData.Stats, false, true);
                          },
                        ),

                        //flow
                        GestureDetector(
                          child: Container(
                            width: otherScreenMiniBtnWidth,
                            height: otherScreenMiniBtnHeight,
                            decoration: new BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                  otherScreenBorderRadius),
                              color: otherScreenBtnColor,
                              border: new Border.all(
                                  color: otherScreenBorderColor,
                                  width: otherScreenBorderWidth),
                            ),
                            child: Stack(
                              children: <Widget>[
                                Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    height: otherScreenImgHeight,
                                    width: otherScreenImgWidth,
                                    child: graph,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          onTap: () {
                            widget.onArgumentSaving({
                              "matchID": matchId,
                              "player1": player1Id,
                              "player2": player2Id,
                              "date": dateObject.toString(),
                              "time": timeInSeconds,
                            });
                            if (Const.buttonCountflow < 9) {
                              Const.buttonCountflow = Const.buttonCountflow + 1;
                            } else {
                              Const.buttonCountflow = 0;
                            }
                            widget.onSelected(RoutingData.Flow, false, true);
                          },
                        ),

                        //game setting update
                        GestureDetector(
                          onTap: () {
                            // ( int setPageVal, bool bottomBarShowHide, bool backArrowShowHide )
                            widget.onSelected(
                                RoutingData.GameSettingUpdate, false, true);
                          },
                          child: Container(
                            width: otherScreenMiniBtnWidth,
                            height: otherScreenMiniBtnHeight,
                            decoration: new BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                  otherScreenBorderRadius),
                              color: otherScreenBtnColor,
                              border: new Border.all(
                                  color: otherScreenBorderColor,
                                  width: otherScreenBorderWidth),
                            ),
                            child: Stack(
                              children: <Widget>[
                                Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    height: otherScreenImgHeight,
                                    width: otherScreenImgWidth,
                                    child: settings,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),

                        //undo
                        GestureDetector(
                          onTap: () {
                            if (undoBtn) {
                              selectScoreData();
                            }

                            checkEnabled();
                          },
                          child: Container(
                            width: otherScreenMiniBtnWidth,
                            height: otherScreenMiniBtnHeight,
                            decoration: new BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                  otherScreenBorderRadius),
                              color: undoBtn
                                  ? otherScreenBtnColor
                                  : AppColors.gray,
                              border: new Border.all(
                                  color: undoBtn
                                      ? otherScreenBorderColor
                                      : AppColors.white,
                                  width: otherScreenBorderWidth),
                            ),
                            child: Stack(
                              children: <Widget>[
                                Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    height: otherScreenImgHeight,
                                    width: otherScreenImgWidth,
                                    child: undo,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),

                        //delete
                        GestureDetector(
                          onTap: () {
                            showDialog(
                                context: context,
                                builder: (context) => AppPopup(
                                    context,
                                    ScoreValues.delete,
                                    [ScoreValues.delete],
                                    timerPauseCallback));

                            checkEnabled();
                          },
                          child: Container(
                            width: otherScreenMiniBtnWidth,
                            height: otherScreenMiniBtnHeight,
                            decoration: new BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                  otherScreenBorderRadius),
                              color: otherScreenBtnColor,
                              border: new Border.all(
                                  color: otherScreenBorderColor,
                                  width: otherScreenBorderWidth),
                            ),
                            child: Stack(
                              children: <Widget>[
                                Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    height: otherScreenImgHeight,
                                    width: otherScreenImgWidth,
                                    child: delete,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ])),
        ),
      ),
      onWillPop: () async => Future.value(false),
    );
  }

  @override
  resultFunction(func, subFunc, response) {
    switch (func) {
      case ControllerFunc.db_sqlite:
        {
          if (response['response_state'] == true) {
            switch (response['calledMethod']) {
              case 'selectScoreData':
                setState(() {
                  undoBtn = false;
                });
                if (response['response_data'].toString() != "null" &&
                    response['response_data'].toString() != "[]") {
                  // Do undo function when there is at least two data
                  if (response['response_data'].length == 2) {
                    // print(response['response_data']);
                    setState(() {
                      if (response['response_data'][1]['setFinished'] != 1 &&
                          response['response_data'][0]['type'] !=
                              ScoreValues.flt) {
                        if (response['response_data'][1]['serve'] == 1) {
                          player1serve = true;
                          player2serve = false;
                          widget.funcSetplayerServe(1);
                        } else {
                          player2serve = true;
                          player1serve = false;
                          widget.funcSetplayerServe(2);
                        }
                      }

                      // noOfTries = response['response_data'][1]['rally'];
                      noOfTries = 1;

                      player1noOfSets =
                          response['response_data'][1]['player1Sets'];
                      player2noOfSets =
                          response['response_data'][1]['player2Sets'];

                      if (response['response_data'][1]['setFinished'] == 1) {
                        player1noOfGames = 0;
                        player2noOfGames = 0;
                        player1noOfPoints = 0;
                        player2noOfPoints = 0;
                      } else {
                        player1noOfGames =
                            response['response_data'][1]['player1Games'];
                        player2noOfGames =
                            response['response_data'][1]['player2Games'];
                        player1noOfPoints =
                            response['response_data'][1]['player1Points'];
                        player2noOfPoints =
                            response['response_data'][1]['player2Points'];
                      }

                      totalSets = response['response_data'][1]['totalSets'];
                      totalGames = response['response_data'][1]['totalGames'];
                      setChangeDetector =
                          response['response_data'][1]['setFinished'];
                      tieBreakOn = response['response_data'][1]['tieBreakOn'];
                      player1inAd = false;
                      player2inAd = false;

                      if (response['response_data'][1]['player1Points'] == 41) {
                        player1inAd = true;
                      }

                      if (response['response_data'][1]['player2Points'] == 41) {
                        player2inAd = true;
                      }

                      breakProcessToTieBreak =
                          response['response_data'][1]['player2Points'] == 1
                              ? true
                              : false;

                      semiAdvantageSecond = response['response_data'][1]
                                  ['semiAdvantageSecond'] ==
                              1
                          ? true
                          : false;

                      // print("Player1Points: $player1noOfPoints");
                      // print("Player1Games: $player1noOfGames");
                      // print("Player1Sets: $player1noOfSets");

                      // print("Player2Points: $player2noOfPoints");
                      // print("Player2Games: $player2noOfGames");
                      // print("Player2Sets: $player2noOfSets");
                      // print("totalSets: $totalSets");
                      // print("totalGames: $totalGames");
                      // print("---------------");
                      // print(response['response_data']);
                      // print("daameed");

                      setState(() {
                        switch (response['response_data'][1]['totalSets']) {
                          case 0:
                            hideAllTiles();
                            break;
                          case 1:
                            hideAllTiles();
                            i1sAngle1Status = true;
                            i2sAngle1Status = true;
                            break;
                          case 2:
                            hideAllTiles();
                            i1sAngle1Status = true;
                            i2sAngle1Status = true;
                            i1sAngle2Status = true;
                            i2sAngle2Status = true;
                            break;
                          case 3:
                            hideAllTiles();
                            i1sAngle1Status = true;
                            i2sAngle1Status = true;
                            i1sAngle2Status = true;
                            i2sAngle2Status = true;
                            i1sAngle3Status = true;
                            i2sAngle3Status = true;
                            break;
                          case 4:
                            hideAllTiles();
                            i1sAngle1Status = true;
                            i2sAngle1Status = true;
                            i1sAngle2Status = true;
                            i2sAngle2Status = true;
                            i1sAngle3Status = true;
                            i2sAngle3Status = true;
                            i1sAngle4Status = true;
                            i2sAngle4Status = true;
                            break;
                          case 5:
                            hideAllTiles();
                            i1sAngle1Status = true;
                            i2sAngle1Status = true;
                            i1sAngle2Status = true;
                            i2sAngle2Status = true;
                            i1sAngle3Status = true;
                            i2sAngle3Status = true;
                            i1sAngle4Status = true;
                            i2sAngle4Status = true;
                            i1sAngle5Status = true;
                            i2sAngle5Status = true;
                            break;
                          default:
                        }
                      });

                      // Set the type of fault defalut and other types
                      // 0 means new item
                      // 1 means old item

                      // think like this the time we edit is new item and previous one is older than now
                      if (response['response_data'][0]['type'] ==
                          ScoreValues.flt) {
                        // print("fault");
                        if (response['response_data'][0]['won'] == player1Id) {
                          player1FaultError = ScoreValues.flt;
                          player2Type = ScoreValues.dflt;
                        } else {
                          player2FaultError = ScoreValues.flt;
                          player1Type = ScoreValues.dflt;
                        }
                      } else if (response['response_data'][0]['type'] ==
                          ScoreValues.dflt) {
                        // print("double");
                        if (response['response_data'][1]['won'] == player1Id) {
                          player1FaultError = ScoreValues.dflt;
                          player2Type = ScoreValues.flt;
                        } else {
                          player2FaultError = ScoreValues.dflt;
                          player1Type = ScoreValues.flt;
                        }
                      } else {
                        // print("other");
                        if (response['response_data'][1]['type'] ==
                            ScoreValues.flt) {
                          if (response['response_data'][1]['won'] ==
                              player1Id) {
                            player1FaultError = ScoreValues.dflt;
                            player2Type = ScoreValues.flt;
                          } else {
                            player2FaultError = ScoreValues.dflt;
                            player1Type = ScoreValues.flt;
                          }
                        }
                      }

                      //----------------------------------------------------------

                      if (response['response_data'][1]['player1Points'] == 41) {
                        player1inAd = true;
                        player2inAd = false;
                      } else {
                        player1inAd = false;
                      }

                      if (response['response_data'][1]['player2Points'] == 41) {
                        player1inAd = false;
                        player2inAd = true;
                      } else {
                        player2inAd = false;
                      }
                    });
                    deleteScoreData(response['response_data'][0]['id']);
                  } else {
                    deleteScoreData(response['response_data'][0]['id']);
                    player1serve = true;
                    player2serve = false;
                    widget.funcSetplayerServe(1);
                    noOfTries = 1;
                    player1noOfSets = 0;
                    player1noOfGames = 0;
                    player1noOfPoints = 0;
                    player2noOfSets = 0;
                    player2noOfPoints = 0;
                    player2noOfGames = 0;
                    totalSets = 0;
                    totalGames = 0;
                    player1inAd = false;
                    player2inAd = false;
                    hideAllTiles();
                    // print('ddd');
                  }
                  setItemsTemp();
                } else {
                  // print('Response is null');
                }
                break;

              case 'updateScoreData':
                setItemsTemp();
                break;

              case 'faultToDatabase':
                setItemsTemp();
                break;

              case 'getPlayer1Data':
                if (response['response_data'].length == 1) {
                  setState(() {
                    playerImage1 = response['response_data'][0]['player_image'];
                    player1Name = response['response_data'][0]['name'];

                    int x = response['response_data'][0]['Player_no'];
                    if (x < 10) {
                      player1No = "0" + x.toString();
                    } else {
                      player1No = x.toString();
                    }
                  });
                }
                break;

              case 'getPlayer2Data':
                if (response['response_data'].length == 1) {
                  setState(() {
                    playerImage2 = response['response_data'][0]['player_image'];
                    player2Name = response['response_data'][0]['name'];

                    int x = response['response_data'][0]['Player_no'];
                    if (x < 10) {
                      player2No = "0" + x.toString();
                    } else {
                      player2No = x.toString();
                    }
                  });
                }
                break;

              case 'deleteAllData':
                resetItemsTemp();
                widget.toggleStartState(true);
                //widget.onPageChanged(RoutingData.Home, true, true);
                Navigator.pushReplacementNamed(context, "/MainPage");

                break;

              case 'setEndOfTheGame':
                resetItemsTemp();
                widget.toggleStartState(true);
                Const.edOfGame = true;
                // widget.onPageChanged(RoutingData.Home, true, true);
                Navigator.pushReplacementNamed(context, "/MainPage");

                break;

              case 'printMe':
                // print(response['response_data'].toString());
                break;

              case 'setTimeData':
                break;

              default:
            }
          } else {
            // print('Data Base Error');
          }
          break;
        }
      default:
        {
          // print('NOT SQLITE METHOD');
        }
    }
  }
}
