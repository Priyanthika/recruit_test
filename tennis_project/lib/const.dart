import 'package:uuid/uuid.dart';

class Const {
  //Returns a unique file name to save on server
  static String getFileName(String filePath) {
    String path = "";

    //Set File name (Unique name)
    List<String> pathString = filePath.split('/');
    var _file_name =
        getUniqueID() + '.' + pathString[pathString.length - 1].split('.')[1];
    pathString[pathString.length - 1] = _file_name;
    for (final item in pathString) {
      if (pathString[0] != item) {
        path += "/" + item;
      }
    }
    return path;
  }

  //Generate a Unique ID
  static String getUniqueID() {
    return Uuid().v1();
  }

  /// ***Database Details******/
  //Application Database Name
  static String db_name = "sample.db";

  //Application Database type
  static int db_Object = 990;
  static int db_SQLite = 991;

  //Database tables for SQLites
  static List<String> db_tables = [
    'CREATE TABLE IF NOT EXISTS user (name TEXT, age INTEGER, email TEXT, password TEXT)',
  ];

  //Serevr URLs
  static String serverAddr = "http://192.168.1.9/http_req/";
  static String serevrUploadFilesLoc = "uploads/";

  // ******* DATABASE DETAILS TO TENNIZO *****
  //database name to tennizo
  static String dbTennizo = "tennizo.db";

  //Application Database type
  static int dbTennizoSQLite = 991;

  //Database tables for SQLites
  static List<String> dbTennizoTables = [
    'CREATE TABLE IF NOT EXISTS user (User_no INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT,user_name TEXT,password TEXT,gender TEXT, date_of_birth TEXT, user_type TEXT, user_image TEXT, is_login INTEGER)',
    'CREATE TABLE IF NOT EXISTS player (user_name TEXT, player_id TEXT, Player_no INTEGER, name TEXT, gender TEXT, date_of_birth TEXT, handedness TEXT, playstyle TEXT, team TEXT, role TEXT, player_image TEXT, status INTEGER,PRIMARY KEY(user_name,Player_no))',
    'CREATE TABLE IF NOT EXISTS gameSettings (user_name TEXT, match_id TEXT, match_name TEXT, player1 TEXT , player2 TEXT, no_of_games TEXT,  no_of_sets TEXT , wheather TEXT , temperature INTEGER , advantage TEXT , tie_break TEXT , stadium TEXT , court_number TEXT ,game_date TEXT , game_time TEXT)',
    'CREATE TABLE IF NOT EXISTS stadium (user_name TEXT, stadium_id TEXT, stadium_name TEXT, stadium_address TEXT , contact_number String, no_of_courts INTEGER, reservation_url TEXT , stadium_image TEXT, Stadium_Lat DOUBLE, Stadium_Lon DOUBLE ,  status INTEGER)',
    'CREATE TABLE IF NOT EXISTS court (court_id TEXT,stadium_id TEXT,court_name TEXT, serface TEXT , indoor TEXT, available_time TEXT, stadium_name TEXT, court_image, status INTEGER)',
    'CREATE TABLE IF NOT EXISTS scoreData (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,match_id TEXT, won TEXT, type TEXT, serve INTEGER, rally INTEGER, player1Sets INTEGER, player1Games INTEGER, player1Points INTEGER, player2Sets INTEGER, player2Games INTEGER, player2Points INTEGER, totalSets INTEGER, totalGames INTEGER, gameFinished INTEGER, setFinished INTEGER, tieBreakOn INTEGER, gameTime INTEGER , serveChance INTEGER, semiAdvantageSecond INTEGER , breakProcessToTieBreak INTEGER)',
    'CREATE TABLE IF NOT EXISTS result (result_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,match_id TEXT, game_time TEXT, winner TEXT, looser TEXT, winner_set TEXT, looser_set TEXT, game_Status INTEGER, game_tot_time INTEGER)',
  ];

// Add display count in stats
  static int buttonCount = 0;
  // Add display count in flow
  static int buttonCountflow = 0;
  // Add display status in home
  static bool edOfGame = false;
  //timer value
  static bool timeChange = true;

  //MAP
  static String mapsAPIKey = "AIzaSyByn7QSqTUA-jsFb4AWY_nF_Dk14FNXKps";
}
